﻿using System;

namespace DiGraphAttribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class StartClassAttribute : Attribute
    {

    }
}
