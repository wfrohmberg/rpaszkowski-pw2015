﻿using System;
using System.Collections.Generic;

namespace DiGraph
{
    /// <summary>
    /// Class implementing iterative mergesort algorithm
    /// for sorting generic list.
    /// </summary>
    /// <typeparam name="T">Any type implementing the IComparable&lt;T&gt; interface</typeparam>
    public static class MergeSort<T> where T:IComparable<T>
    {
        /// <summary>
        /// Sorts elements in list according to iterative (non-recursive, bottom-top)
        /// mergesort algorithm. It uses comparer to sort items.
        /// </summary>
        /// <param name="list">List of items to sort.</param>
        /// <param name="comparer">Comparer that will compare two items from list.</param>
        public static void Sort(IList<T> list, IComparer<T> comparer)
        {
            if (list.Count < 2)
                return;

            for (int i = 1; i < list.Count; i *= 2)
            {
                for (int j = i; j < list.Count; j += 2 * i)
                {
                    Merge(list, j - i, j, Math.Min(i + j, list.Count), comparer);
                }
            }
        }

        /// <summary>
        /// Merge step of mergesort algorithm. It merges two sublists of list
        /// to a new sorted list.
        /// </summary>
        /// <param name="list">List of items to from which we take sublists to merge.</param>
        /// <param name="start">Start index of the first sublist.</param>
        /// <param name="middle">Middle index that is the end of first sublist and beggining of the second one.</param>
        /// <param name="end">End of the second sublist.</param>
        /// <param name="comparer">Comparer that will compare two items from list.</param>
        private static void Merge(IList<T> list, int start, int middle, int end, IComparer<T> comparer)
        {
            IList<T> result = new List<T>();
            int l = start, r = middle;

            while (l < middle && r < end)
            {
                result.Add(comparer.Compare(list[l], list[r]) < 0 ? list[l++] : list[r++]);
            }

            while (r < end) result.Add(list[r++]);
            while (l < middle) result.Add(list[l++]);

            for (int i = 0; i < result.Count; i++)
                list[start + i] = result[i];
        }
    }
}
