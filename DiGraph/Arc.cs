﻿using System;
using System.Runtime.Serialization;

namespace DiGraph
{
    /// <summary>
    /// An arc of a graph
    /// </summary>
    /// <typeparam name="TNodeType">Type of a node</typeparam>
    /// <typeparam name="TArcType">Type of an arc</typeparam>
    [DataContract(Name = "Arc", Namespace = "Graph", IsReference = true)]
    public class Arc<TNodeType, TArcType> : IComparable<Arc<TNodeType, TArcType>>
    {
        /// <summary>
        /// A node from which this arc starts
        /// </summary>
        [DataMember(Name = "FromNode")]
        public Node<TNodeType, TArcType> From { get; set; }

        /// <summary>
        /// A node where this arc ends
        /// </summary>
        [DataMember(Name = "ToNode")]
        public Node<TNodeType, TArcType> To { get; set; }

        /// <summary>
        /// A value of the arc
        /// </summary>
        [DataMember]
        public TArcType Value { get; set; }

        /// <summary>
        /// Default constructor for serialization
        /// </summary>
        public Arc()
        {
            From = null;
            To = null;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">A value of the arc</param>
        /// <param name="from">A node from which this arc starts</param>
        /// <param name="to">A node where this arc ends</param>
        public Arc(TArcType value, Node<TNodeType, TArcType> from = null, Node<TNodeType, TArcType> to = null)
        {
            Value = value;
            From = from;
            To = to;
        }

        /// <summary>
        /// A method for comparing arcs
        /// </summary>
        /// <param name="other">An other arc</param>
        /// <returns>Returns an indication of their relative values</returns>
        public int CompareTo(Arc<TNodeType, TArcType> other)
        {
            return To.Id.CompareTo(other.To.Id);
        }

        /// <summary>
        /// Implicit constructor for creating arc using only value
        /// </summary>
        /// <param name="value">A value of the arc</param>
        /// <returns>A newly created arc</returns>
        public static implicit operator Arc<TNodeType, TArcType>(TArcType value)
        {
            return new Arc<TNodeType, TArcType>(value);
        }
    }
}
