﻿using System;
using System.Collections.Generic;

namespace DiGraph
{
    public interface IDiGraphInterface<TNodeType, TArcType>
    {
        Node<TNodeType, TArcType> this[int index]
        {
            get;
            set;
        }

        Arc<TNodeType, TArcType> this[int index1, int index2]
        {
            get;
            set;
        }

        IEnumerable<Node<TNodeType, TArcType>>
             NodesWhere(Predicate<Node<TNodeType, TArcType>> predicate);
        IEnumerable<Arc<TNodeType, TArcType>>
             ArcsWhere(Predicate<Arc<TNodeType, TArcType>> predicate);

        void Rebuild();
    }
}
