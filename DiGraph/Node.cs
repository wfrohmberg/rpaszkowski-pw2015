﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace DiGraph
{
    /// <summary>
    /// A node of a graph
    /// </summary>
    /// <typeparam name="TNodeType">Type of a node</typeparam>
    /// <typeparam name="TArcType">Type of an arc</typeparam>
    [DataContract(Name = "Node", Namespace = "Graph", IsReference = true)]
    public class Node<TNodeType, TArcType> : IEnumerable<Arc<TNodeType, TArcType>>
    {
        /// <summary>
        /// Boolean value to check if arcs are sorted
        /// </summary>
        [IgnoreDataMember]
        public bool Dirty { get; set; }

        /// <summary>
        /// Unique id number of the node
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Value of the node
        /// </summary>
        [DataMember]
        public TNodeType Value { get; set; }

        /// <summary>
        /// List of arcs which belong to this node
        /// </summary>
        [DataMember(Name = "Arcs")]
        public List<Arc<TNodeType, TArcType>> Arcs { get; set; }

        /// <summary>
        /// Number of arcs connected to this node
        /// </summary>
        [DataMember]
        public int Degree;

        /// <summary>
        /// Indexer for arcs
        /// </summary>
        /// <param name="to">A node to which the arc points</param>
        /// <returns>The arc for which we are looking for</returns>
        public Arc<TNodeType, TArcType> this[Node<TNodeType, TArcType> to]
        {
            get
            {
                if (to == null)
                    return null;
                return FindArc(to);
            }
            set
            {
                if (value != null && to != this)
                {
                    Arc<TNodeType, TArcType> arc = FindArc(to);
                    if (arc == null) 
                    {
                        if (value.From == null && value.To == null)
                        {
                            arc = new Arc<TNodeType, TArcType>(value.Value, this, to);
                            Arcs.Add(arc);
                        }
                        else
                        {
                            arc = value;
                            Arcs.Add(arc);
                        }

                        Degree++;
                        arc.To.Degree++;

                        Dirty = true;
                    }
                    else
                    {
                        arc.Value = value.Value;
                    }
                }
                else if (value == null)
                {
                    Arc<TNodeType, TArcType> arc = FindArc(to);
                    if (arc != null)
                    {
                        Arcs.Remove(arc);
                        Degree--;
                        arc.To.Degree--;
                        Dirty = true;
                    }
                }
            }
        }

        /// <summary>
        /// Simple constructor
        /// </summary>
        /// <param name="id">A unique id of the node</param>
        public Node(int id = 0)
        {
            Id = id;
            Dirty = false;
            Arcs = new List<Arc<TNodeType, TArcType>>();
        }

        /// <summary>
        /// Simple constructor
        /// </summary>
        /// <param name="value">A value of the node</param>
        /// <param name="id">A unique id of the node</param>
        public Node(TNodeType value, int id = 0) : this(id)
        {
            Value = value;
        }

        /// <summary>
        /// Finds specified arc
        /// </summary>
        /// <param name="to">A node to which the arc points</param>
        /// <returns>The arc for which we are looking for</returns>
        protected Arc<TNodeType, TArcType> FindArc(Node<TNodeType, TArcType> to) {
            if (Arcs.Count == 0)
                return null;

            if (Dirty)
            {
                return Arcs.FirstOrDefault(arc => to.Id == arc.To.Id && arc.To.Equals(to));
            }
            else
            {
                int right = Arcs.Count - 1;
                int left = 0;
                int cur = (left + right) / 2;
                Arc<TNodeType, TArcType> arc = Arcs[cur];

                while (left < right)
                {
                    while (arc.To.Id > to.Id)
                    {
                        right = cur - 1;
                        cur = (left + right) / 2;
                        arc = Arcs[cur];
                    }

                    while (arc.To.Id < to.Id)
                    {
                        left = cur + 1;
                        cur = (left + right) / 2;
                        arc = Arcs[cur];
                    }

                    if (arc.To.Id == to.Id)
                        return arc;
                }

                if (arc.To.Id == to.Id)
                    return arc;
            }
            return null;
        }

        /// <summary>
        /// A method for sorting arcs in this node
        /// </summary>
        public void Sort()
        {
            if (Arcs.Count > 1)
            {
                MergeSort<Arc<TNodeType, TArcType>>.Sort(Arcs, Comparer<Arc<TNodeType, TArcType>>.Default);
                Dirty = false;
            }
        }
    
        /// <summary>
        /// An enumerator for arcs in this node
        /// </summary>
        /// <returns>Enumerator for arcs in this node</returns>
        public IEnumerator<Arc<TNodeType,TArcType>> GetEnumerator()
        {
            return Arcs.GetEnumerator();
        }

        /// <summary>
        /// An enumerator for arcs in this node
        /// </summary>
        /// <returns>Enumerator for arcs in this node</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
 	        return GetEnumerator();
        }

        /// <summary>
        /// Finds arcs that satisfy the predicate
        /// </summary>
        /// <param name="predicate">predicate returning boolean value specifing the arcs for which we are looking for</param>
        /// <returns>List of arcs satisfing the predicate</returns>
        public IEnumerable<Arc<TNodeType, TArcType>> ArcsWhere(Predicate<Arc<TNodeType, TArcType>> predicate)
        {
            List<Arc<TNodeType, TArcType>> result = Arcs.FindAll(predicate);
            return result;
        }

        /// <summary>
        /// Implicit constructor for creating node using only value
        /// </summary>
        /// <param name="value">A value of the node</param>
        /// <returns>A newly created node</returns>
        public static implicit operator Node<TNodeType, TArcType>(TNodeType value)
        {
            return new Node<TNodeType, TArcType>(value);
        }
    }
}
