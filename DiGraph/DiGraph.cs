﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace DiGraph
{
    /// <summary>
    /// Directional graph implementation
    /// </summary>
    /// <typeparam name="TNodeType">Type of a node</typeparam>
    /// <typeparam name="TArcType">Type of an arc</typeparam>
    [DataContract(Name = "DiGraph", Namespace = "Graph")]
    public class DiGraph<TNodeType, TArcType> : IDiGraphInterface<TNodeType, TArcType>
    {
        [DataMember(Name = "Nodes")]
        public List<Node<TNodeType, TArcType>> Nodes { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DiGraph()
        {
            Nodes = new List<Node<TNodeType, TArcType>>();
        }
        
        /// <summary>
        /// Adds a node to the graph
        /// </summary>
        /// <returns>Created node</returns>
        public virtual Node<TNodeType, TArcType> AddNode()
        {
            Node<TNodeType, TArcType> node = new Node<TNodeType, TArcType>(Nodes.Count);
            Nodes.Add(node);
            return node;
        }

        /// <summary>
        /// Adds a node to the graph
        /// </summary>
        /// <param name="value">Value of the node</param>
        /// <returns>Created node</returns>
        public virtual Node<TNodeType, TArcType> AddNode(TNodeType value)
        {
            Node<TNodeType, TArcType> node = AddNode();
            node.Value = value;
            return node;
        }

        /// <summary>
        /// Adds specified node to the graph. If node with the same Id exists then id of the new node is
        /// one higher then the maximum one.
        /// </summary>
        /// <param name="node">Node to add</param>
        public virtual void AddNode(Node<TNodeType, TArcType> node)
        {
            if (!Nodes.Contains(node))
            {
                node.Id = Nodes.Count;
                Nodes.Add(node);
            }
        }

        /// <summary>
        /// Adds specified node with id to the graph 
        /// </summary>
        /// <param name="node">Node to add</param>
        public virtual void InsertNode(Node<TNodeType, TArcType> node)
        {
            if (!Nodes.Contains(node))
            {
                if (node.Id > Nodes.Count)
                    node.Id = Nodes.Count;

                foreach (Node<TNodeType, TArcType> n in NodesWhere(x => x.Id >= node.Id))
                    n.Id++;

                Nodes.Insert(node.Id, node);
            }
        }

        /// <summary>
        /// Removes a node from the graph
        /// </summary>
        /// <param name="node">Node to remove</param>
        public virtual void RemoveNode(Node<TNodeType, TArcType> node)
        {
            int id = Nodes.IndexOf(node);
            if (id == -1)
                return;

            foreach (Arc<TNodeType, TArcType> arc in ArcsWhere(x => x.To.Id == id || x.From.Id == id))
            {
                arc.From.Arcs.Remove(arc);
                arc.To.Arcs.Remove(arc);
            }

            foreach (Node<TNodeType, TArcType> n in NodesWhere(x => x.Id > id))
                n.Id--;

            Nodes.RemoveAt(id);
        }

        /// <summary>
        /// Clears the graph
        /// </summary>
        public void Clear()
        {
            Nodes.Clear();
        }

        /// <summary>
        /// Indexer for nodes
        /// </summary>
        /// <param name="index">A unique id of a node</param>
        /// <returns>A specified node</returns>
        public virtual Node<TNodeType, TArcType> this[int index]
        {
            get
            {
                if (index < Nodes.Count)
                    return Nodes[index];
                else
                    return null;
            }
            set
            {
                if (index < Nodes.Count)
                {
                    Node<TNodeType, TArcType> node = new Node<TNodeType, TArcType>(value.Value, index);
                    Nodes[index] = node;
                }
                else if (index == Nodes.Count)
                {
                    AddNode(value.Value);
                }
            }
        }

        /// <summary>
        /// Indexer for arcs
        /// </summary>
        /// <param name="index1">A unique id for node where the arc starts</param>
        /// <param name="index2">A unique id for node where the arc ends</param>
        /// <returns>An arc for which we are looking for</returns>
        public virtual Arc<TNodeType, TArcType> this[int index1, int index2]
        {
            get 
            {
                if (index1 < Nodes.Count && index1 != index2)
                    return Nodes[index1][Nodes[index2]];
                else
                    return null;
            }
            set 
            {
                if (index1 < Nodes.Count && index1 != index2)
                    Nodes[index1][Nodes[index2]] = value; 
            }
        }

        /// <summary>
        /// Looks for nodes that meet the predicate needs
        /// </summary>
        /// <param name="predicate">predicate returning boolean value specifing wanted nodes</param>
        /// <returns>A list of nodes that meet the predicate needs</returns>
        public IEnumerable<Node<TNodeType, TArcType>> NodesWhere(Predicate<Node<TNodeType, TArcType>> predicate)
        {
            return Nodes.Where(node => predicate(node)).ToList();
        }

        /// <summary>
        /// Looks for arcs that meet the predicate needs
        /// </summary>
        /// <param name="predicate">predicate returning boolean value specifing wanted arcs</param>
        /// <returns>A list of arcs that meet the predicate needs</returns>
        public IEnumerable<Arc<TNodeType, TArcType>> ArcsWhere(Predicate<Arc<TNodeType, TArcType>> predicate)
        {
            return (from node in Nodes from arc in node where predicate(arc) select arc).ToList();
        }

        /// <summary>
        /// Returns all nodes in graph
        /// </summary>
        /// <returns>All nodes in graph</returns>
        public IEnumerable<Node<TNodeType, TArcType>> GetAllNodes()
        {
            return Nodes;
        }

        /// <summary>
        /// Returns all arcs in graph
        /// </summary>
        /// <returns>all arcs in graph</returns>
        public IEnumerable<Arc<TNodeType, TArcType>> GetAllArcs()
        {
            List<Arc<TNodeType, TArcType>> arcs = new List<Arc<TNodeType, TArcType>>();
            foreach (Node<TNodeType, TArcType> node in Nodes)
            {
                arcs.AddRange(node.Arcs);
            }
            return arcs;
        }

        /// <summary>
        /// Rebuilds graph so accessing it's value is more efficient
        /// </summary>
        public void Rebuild()
        {
            foreach(Node<TNodeType, TArcType> node in Nodes)
                node.Sort();
        }
    }
}
