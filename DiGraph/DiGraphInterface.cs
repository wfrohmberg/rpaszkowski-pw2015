﻿using System;
using System.Collections.Generic;

namespace Graph
{
    public interface DiGraphInterface<NodeType, ArcType>
    {
        Node<NodeType, ArcType> this[int index]
        {
            get;
            set;
        }

        Arc<NodeType, ArcType> this[int index1, int index2]
        {
            get;
            set;
        }

        IEnumerable<Node<NodeType, ArcType>>
             NodesWhere(Predicate<Node<NodeType, ArcType>> predicate);
        IEnumerable<Arc<NodeType, ArcType>>
             ArcsWhere(Predicate<Arc<NodeType, ArcType>> predicate);

        void Rebuild();
    }
}
