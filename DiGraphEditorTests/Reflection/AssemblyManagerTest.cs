﻿using System;
using System.Windows;
using DiGraphEditor.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphEditorTests.Reflection
{
    [TestClass()]
    public class AssemblyManagerTest
    {
        [TestInitialize()]
        public void Setup()
        {
            AssemblyManager.GetInstance().Resource = new ResourceDictionary();
        }

        [TestMethod()]
        public void AddNullAssemblyTest()
        {
            AssemblyManager.GetInstance().AddAssembly(null);
            Assert.AreEqual(0, AssemblyManager.GetInstance().GetAllAssemblies().Count);
        }

        [TestMethod()]
        public void RemoveNullAssemblyTest()
        {
            AssemblyManager.GetInstance().RemoveAssembly(null);
            Assert.AreEqual(0, AssemblyManager.GetInstance().GetAllAssemblies().Count);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void AddWrongAssemblyTest()
        {
            AssemblyManager.GetInstance().AddAssembly("Wrong");
        }

        [TestMethod()]
        public void RemoveWrongAssemblyTest()
        {
            AssemblyManager.GetInstance().RemoveAssembly("Wrong");
            Assert.AreEqual(0, AssemblyManager.GetInstance().GetAllAssemblies().Count);
        }
    }
}
