﻿using System.Reflection;
using System.Windows.Controls;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DiGraphEditor;

namespace DiGraphEditorTests.Utils
{
    [TestClass()]
    public class ValueContentCreatorTests
    {
        [TestMethod()]
        public void GetNestedValuesTest()
        {
            Panel panel = new StackPanel();
            Wrapper wrapper = new Wrapper();
            PropertyInfo personInfo = wrapper.GetType().GetProperty("WPerson");
            ValueContentCreator.GetNestedValues(panel, wrapper, personInfo, 2);
            Assert.AreEqual(1, panel.Children.Count);
        }

        [TestMethod()]
        public void EmptyPositionTest()
        {
            Panel posPanel = ValueContentCreator.CreatePositionPanel("Hi", "Water", null);
            Assert.IsNull(posPanel);
        }

        [TestMethod()]
        public void CorrectPositionTest()
        {
            NodeEntity<string, int> node = new NodeEntity<string, int>("Doom");
            Panel posPanel = ValueContentCreator.CreatePositionPanel("Hi", "Water", node);
            Assert.AreEqual(2, posPanel.Children.Count);
        }

        [TestMethod()]
        public void WrongObjectButtonTest()
        {
            AppManager.GetInstance().InitializeCommands(null, null, null, null);
            Wrapper w = new Wrapper();
            Button btn = ValueContentCreator.CreateDeleteButton(w);
            Assert.IsNull(btn);
        }

        [TestMethod()]
        public void ButtonTest()
        {
            AppManager.GetInstance().InitializeCommands(null, null, null, null);
            NodeEntity<string, int> node = new NodeEntity<string, int>("Doom");
            Button btn = ValueContentCreator.CreateDeleteButton(node);
            Assert.IsNotNull(btn);
        }
    }
}
