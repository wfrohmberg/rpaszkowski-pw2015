﻿namespace DiGraphEditorTests
{
    public class Wrapper
    {
        public Person WPerson { get; set; }

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public Address PersonAddress { get; set; }
            public bool Alive { get; set; }

            public class Address
            {
                public string Street;
                public string City;
                public int PostCode;
            }
        }
    }
}