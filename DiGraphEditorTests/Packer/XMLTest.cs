﻿using System;
using System.IO;
using DiGraph;
using DiGraphEditor.Packer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphEditorTests.Packer
{
    [TestClass()]
    public class XmlTest
    {
        private DiGraph<string, int> _graph;

        [TestInitialize()]
        public void Setup()
        {
            _graph = new DiGraph<string, int>();
            _graph[0] = "Zero";
            _graph[1] = "One";
            _graph[2] = "Two";
            _graph[3] = "Three";

            _graph[1, 0] = 5;
            _graph[3, 2] = 2;
            _graph[0, 2] = 5;
            _graph[2, 3] = 32;
            _graph[0, 1] = 6;
            _graph[2, 0] = 12;
            _graph[2, 1] = 43;
        }

        [TestMethod()]
        public void SerializeTest()
        {
            XmlPacker packer = new XmlPacker();
            string result = packer.Serialize(_graph);
            Console.WriteLine(result);
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DeserializeTest()
        {
            XmlPacker packer = new XmlPacker();
            string result = packer.Serialize(_graph);
            DiGraph<string, int> newGraph = packer.Deserialize<DiGraph<string, int>>(result);
            Assert.IsTrue(newGraph.Nodes.Count > 0);
        }

        [TestMethod()]
        public void PackingTest()
        {
            XmlPacker packer = new XmlPacker();
            string xml = packer.Serialize(_graph);
            packer.Pack(Path.GetTempPath() + "//test.zip", xml);
            string result = packer.Unpack(Path.GetTempPath() + "//test.zip");
            Assert.AreEqual(xml, result);
        }
    }
}
