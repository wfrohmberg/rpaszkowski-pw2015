﻿using System;
using System.Collections.Generic;
using DiGraphEditor.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphEditorTests.Commands
{
    [TestClass()]
    public class UndoRedoTest
    {
        private class SimpleCommand : IReversibleCommand
        {
            private readonly int _number;
            private readonly List<int> _result;

            public SimpleCommand(int num, List<int> res)
            {
                _number = num;
                _result = res;
            }

            public void Execute(object parameter)
            {
                Console.WriteLine(@"Execute " + _number);
                _result.Add(_number);
            }

            public void UnExecute()
            {
                Console.WriteLine(@"UnExecute: " + _number);
                _result.Add(_number);
            }

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public IReversibleCommand Clone()
            {
                return null;
            }
        }

        private UndoRedo _undoRedo;
        private List<int> _result;

        [TestInitialize()]
        public void Setup()
        {
            _result = new List<int>();
            _undoRedo = UndoRedo.GetInstance();
            _undoRedo.SetHistorySize(8);
            for (int i = 0; i < 8; i++)
            {
                SimpleCommand simCom = new SimpleCommand(i, _result);
                _undoRedo.AddCommand(simCom);
            }
        }

        [TestCleanup()]
        public void Cleanup()
        {
            _undoRedo.ClearHistory();
            _undoRedo = null;
        }

        [TestMethod()]
        public void UndoTest()
        {
            _undoRedo.Undo();
            _undoRedo.Undo();
            _undoRedo.Undo();

            CollectionAssert.AreEqual(new List<int> { 7, 6, 5 }, _result);
        }

        [TestMethod()]
        public void RedoTest()
        {
            _undoRedo.Undo();
            _undoRedo.Undo();
            _undoRedo.Redo();

            CollectionAssert.AreEqual(new List<int> {7, 6, 6}, _result);
        }

        [TestMethod()]
        public void MaxUndoTest()
        {
            SimpleCommand simCom = new SimpleCommand(8, _result);
            _undoRedo.AddCommand(simCom);

            for (int i = 0; i < 12; i++)
            {
                _undoRedo.Undo();
            }

            CollectionAssert.AreEqual(new List<int> { 8, 7, 6, 5, 4, 3, 2, 1 }, _result);
        }

        [TestMethod()]
        public void MaxRedoTest()
        {
            _undoRedo.Redo();

            Assert.IsTrue(_result.Count == 0);
        }
    }
}
