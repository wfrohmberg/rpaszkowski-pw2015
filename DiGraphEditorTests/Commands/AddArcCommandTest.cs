﻿using System.Reflection;
using System.Windows.Controls;
using DiGraphEditor.Commands;
using DiGraphEditor.GraphVisualizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphEditorTests.Commands
{
    [TestClass()]
    public class AddArcCommandTest
    {
        private Grid _grid;
        private AddArcCommand _command;

        [TestInitialize()]
        public void Setup()
        {
            _grid = new Grid();
            _command = new AddArcCommand(_grid, null);
            UndoRedo.GetInstance().Enabled = true;
        }

        [TestMethod()]
        public void AddArcTest()
        {
            AddArcCommand command = new AddArcCommand(null, null);
            Assert.IsNotNull(command);
        }

        [TestMethod()]
        public void NullParameterExecuteTest()
        {
            if (_command.CanExecute(null))
                _command.Execute(null);

            Assert.AreEqual(0, _grid.Children.Count);
        }

        [TestMethod()]
        public void TwoStepAddTest()
        {
            NodeEntity<string, int> from = new NodeEntity<string, int>();
            NodeEntity<string, int> to = new NodeEntity<string, int>(1);

            //command.Execute(from);
            //command.Execute(to);
            //Assert.AreEqual(from.Arcs[0].To.Id, to.Id);
        }

        [TestMethod()]
        public void OneStepAddTest()
        {
            NodeEntity<string, int> from = new NodeEntity<string, int>();
            NodeEntity<string, int> to = new NodeEntity<string, int>(1);

            ArcEntity<string, int> arc = new ArcEntity<string, int>(from) {To = to};

            FieldInfo fieldInfo = _command.GetType().GetField("_arc", BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
                fieldInfo.SetValue(_command, arc);

            if (_command.CanExecute(null))
                _command.Execute(null);
            Assert.AreEqual(2, _grid.Children.Count);
        }

        [TestMethod()]
        public void UnexecuteTest()
        {
            NodeEntity<string, int> from = new NodeEntity<string, int>();
            NodeEntity<string, int> to = new NodeEntity<string, int>(1);

            ArcEntity<string, int> arc = new ArcEntity<string, int>(from) {To = to};

            FieldInfo fieldInfo = _command.GetType().GetField("_arc", BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
                fieldInfo.SetValue(_command, arc);

            if (_command.CanExecute(null))
                _command.Execute(null);
            _command.UnExecute();
            Assert.AreEqual(0, _grid.Children.Count);
        }

        [TestMethod()]
        public void CannotJoinTest()
        {
            Assert.IsFalse(_command.CanJoin());
        }

        [TestMethod()]
        public void CanJoinTest()
        {
            NodeEntity<string, int> from = new NodeEntity<string, int>();
            ArcEntity<string, int> arc = new ArcEntity<string, int>(from);

            FieldInfo fieldInfo = _command.GetType().GetField("_arc", BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
                fieldInfo.SetValue(_command, arc);

            Assert.IsTrue(_command.CanJoin());
        }
    }
}