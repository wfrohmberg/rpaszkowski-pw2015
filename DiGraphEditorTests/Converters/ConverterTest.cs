﻿using DiGraphEditor.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphEditorTests.Converters
{
    [TestClass()]
    public class ConverterTest
    {
        private BoolToIndexConverter _converter;

        [TestInitialize()]
        public void Setup()
        {
            _converter = new BoolToIndexConverter();
        }

        [TestMethod()]
        public void ConvertTest()
        {
            int falseValue = (int)_converter.Convert(false, null, null, null);
            int trueValue = (int)_converter.Convert(true, null, null, null);
            Assert.IsTrue(falseValue == 1 && trueValue == 0);
        }

        [TestMethod()]
        public void ConvertBackTest()
        {
            bool falseValue = (bool)_converter.ConvertBack(1, null, null, null);
            bool trueValue = (bool)_converter.ConvertBack(0, null, null, null);
            Assert.IsTrue(!falseValue && trueValue);
        }
    }
}
