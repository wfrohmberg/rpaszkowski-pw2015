﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graph;
using DiGraphEditor.Packer;

namespace DiGraphTest
{
    [TestClass]
    public class XMLTest
    {
        private DiGraph<string, int> graph;

        [TestInitialize]
        public void Setup()
        {
            graph = new DiGraph<string, int>();
            graph[0] = "Zero";
            graph[1] = "One";
            graph[2] = "Two";
            graph[3] = "Three";

            graph[1, 0] = 5;
            graph[3, 2] = 2;
            graph[0, 2] = 5;
            graph[2, 3] = 32;
            graph[0, 1] = 6;
            graph[2, 0] = 12;
            graph[2, 1] = 43;
        }

        [TestMethod]
        public void SerializeTest()
        {
            XmlPacker<DiGraph<string, int>> packer = new XmlPacker<DiGraph<string, int>>();
            string result = packer.Serialize(graph);
            Console.WriteLine(result);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeserializeTest()
        {
            XmlPacker<DiGraph<string, int>> packer = new XmlPacker<DiGraph<string, int>>();
            string result = packer.Serialize(graph);
            DiGraph<string, int> newGraph = packer.Deserialize(result);
            Assert.IsTrue(newGraph.Nodes.Count > 0);
        }
    }
}
