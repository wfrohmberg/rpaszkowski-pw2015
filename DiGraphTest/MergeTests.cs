﻿using System;
using System.Collections.Generic;
using DiGraph;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphTest
{
    [TestClass()]
    public class MergeTests
    {
        /// Check if random int list is sorted by merge sort 
        /// by comparing this list with another list sorted by
        /// built-in sorting algorithm.
        [TestMethod()]
        public void MergeRandomInt()
        {
            List<int> list = new List<int>();
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                list.Add(rand.Next(-1000, 1000));
            }

            List<int> expected = new List<int>(list);

            MergeSort<int>.Sort(list, Comparer<int>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i] + @":" + expected[i]);
                Assert.AreEqual(list[i], expected[i]);
            }
        }

        /// Check if string list is sorted by merge sort 
        /// by comparing this list with another list sorted by
        /// built-in sorting algorithm.
        [TestMethod()]
        public void MergeString()
        {
            List<string> list = new List<string> {"abc", "fcs", "kapca", "pojax", "cdes", "xa", "podf"};

            List<string> expected = new List<string>(list);

            MergeSort<string>.Sort(list, Comparer<string>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i] + @":" + expected[i]);
                Assert.AreEqual(list[i], expected[i]);
            }
        }

        /// Check if empty list is not giving any
        /// errors in merge sort algorithm.
        [TestMethod()]
        public void EmptyMerge()
        {
            List<int> list = new List<int>();
            List<int> expected = new List<int>(list);

            MergeSort<int>.Sort(list, Comparer<int>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i] + @":" + expected[i]);
                Assert.AreEqual(list[i], expected[i]);
            }
        }

        /// Class for testing merge sort algorithm with 
        /// classes implementing IComparable interface.
        private class TestInt : IComparable<TestInt>
        {
            private readonly int _val;
            public int Val
            {
                get { return _val; }
            }

            public TestInt(int value)
            {
                _val = value;
            }

            public int CompareTo(TestInt other)
            {
                return (_val < other._val ? -1 : _val == other._val ? 0 : 1);
            }
        }

        /// Check if merge sort can handle objects
        /// implementing the IComparable interface.
        [TestMethod()]
        public void ClassMerge()
        {
            List<TestInt> list = new List<TestInt>();
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new TestInt(rand.Next(1000)));
            }
            List<TestInt> expected = new List<TestInt>(list);

            MergeSort<TestInt>.Sort(list, Comparer<TestInt>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i].Val + @":" + expected[i].Val);
                Assert.AreEqual(list[i].Val, expected[i].Val);
            }
        }
    }
}
