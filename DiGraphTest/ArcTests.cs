﻿using DiGraph;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphTest
{
    [TestClass()]
    public class ArcTest
    {
        private Arc<string, int> _firstArc;
        private Arc<string, int> _secondArc;
        private Arc<string, int> _emptyArc;

        [TestInitialize()]
        public void Setup()
        {
            Node<string, int> from = new Node<string,int>("Hello ");
            Node<string, int> to = new Node<string,int>("wordl!", 1);
            _firstArc = new Arc<string, int>(2, from, to);
            _secondArc = 20;
            _secondArc.From = to;
            _secondArc.To = from;
            _emptyArc = new Arc<string, int>();
        }

        [TestMethod()]
        public void CompareTest()
        {
            Assert.AreEqual(1, _firstArc.CompareTo(_secondArc));
        }

        [TestMethod()]
        public void EmptyArcTest()
        {
            Assert.IsNull(_emptyArc.From);
        }
    }
}
