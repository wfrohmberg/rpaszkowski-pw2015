﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Graph;

namespace DiGraphTest
{
    [TestClass]
    public class MergeUnitTest
    {
        [TestMethod]
        /// Check if random int list is sorted by merge sort 
        /// by comparing this list with another list sorted by
        /// built-in sorting algorithm.
        public void MergeRandomInt()
        {
            List<int> list = new List<int>();
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                list.Add(rand.Next(-1000, 1000));
            }

            List<int> expected = new List<int>(list);

            MergeSort<int>.Sort(list, Comparer<int>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i] + ":" + expected[i]);
                Assert.AreEqual<int>(list[i], expected[i]);
            }
        }

        [TestMethod]
        /// Check if string list is sorted by merge sort 
        /// by comparing this list with another list sorted by
        /// built-in sorting algorithm.
        public void MergeString()
        {
            List<string> list = new List<string>();
            list.Add("abc");
            list.Add("fcs");
            list.Add("kapca");
            list.Add("pojax");
            list.Add("cdes");
            list.Add("xa");
            list.Add("podf");

            List<string> expected = new List<string>(list);

            MergeSort<string>.Sort(list, Comparer<string>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i] + ":" + expected[i]);
                Assert.AreEqual<string>(list[i], expected[i]);
            }
        }

        [TestMethod]
        /// Check if empty list is not giving any
        /// errors in merge sort algorithm.
        public void EmptyMerge()
        {
            List<int> list = new List<int>();
            List<int> expected = new List<int>(list);

            MergeSort<int>.Sort(list, Comparer<int>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i] + ":" + expected[i]);
                Assert.AreEqual<int>(list[i], expected[i]);
            }
        }

        /// <summary>
        /// Class for testing merge sort algorithm with 
        /// classes implementing IComparable<> interface.
        /// </summary>
        private class TestInt : IComparable<TestInt>
        {
            private int val;
            public int Val
            {
                get { return val; }
            }

            public TestInt(int value)
            {
                val = value;
            }

            public int CompareTo(TestInt other)
            {
                return (val < other.val ? -1 : val == other.val ? 0 : 1);
            }
        }

        [TestMethod]
        /// Check if merge sort can handle objects
        /// implementing the IComparable<> interface.
        public void ClassMerge()
        {
            List<TestInt> list = new List<TestInt>();
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new TestInt(rand.Next(1000)));
            }
            List<TestInt> expected = new List<TestInt>(list);

            MergeSort<TestInt>.Sort(list, Comparer<TestInt>.Default);
            expected.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i].Val + ":" + expected[i].Val);
                Assert.AreEqual<int>(list[i].Val, expected[i].Val);
            }
        }
    }
}
