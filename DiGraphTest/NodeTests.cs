﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DiGraph;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphTest
{
    [TestClass()]
    public class NodeTests
    {
        private Node<string, int> _node;
        private List<Node<string, int>> _nodes;

        [TestInitialize()]
        public void Setup()
        {
            _nodes = new List<Node<string, int>>();
            _node = new Node<string, int>("Node 0");
            _nodes.Add(_node);

            for (int i = 1; i < 20; i++)
            {
                Node<string, int> temp = new Node<string, int>("Node " + i, i);
                _nodes.Add(temp);
            }

            for (int i = 19; i >= 1; i--)
            {
                _node[_nodes[i]] = i;
            }
        }

        [TestMethod()]
        public void ChangeArcTest()
        {
            Random rand = new Random();
            int cur = _node[_nodes[5]].Value;
            int temp = rand.Next();
            while (temp == cur) temp = rand.Next(0);

            Console.WriteLine(_node[_nodes[5]].Value);

            _node[_nodes[5]] = new Arc<string, int>(temp);

            Console.WriteLine(_node[_nodes[5]].Value);

            Assert.IsTrue(_node[_nodes[5]].Value == temp && _node[_nodes[5]].Value != cur);
        }

        [TestMethod()]
        public void FindNotJoinedNodeTest()
        {
            Assert.IsNull(_node[new Node<string, int>("I don't know you!", 20)]);
        }

        [TestMethod()]
        public void SortArcsInNodeTest()
        {
            List<int> list = new List<int>();
            foreach (Arc<string, int> arc in _node)
            {
                Console.Write(arc.Value + @" ");
                list.Add(arc.Value);
            }
            
            _node.Sort();
            list.Sort();

            IEnumerator<Arc<string, int>> enumerator = _node.GetEnumerator();
            Console.WriteLine(@" ");

            int count = 0;

            while (enumerator.MoveNext())
            {
                Console.Write(enumerator.Current.Value + @" ");
                Assert.AreEqual(enumerator.Current.Value, list[count]);
                count++;
            }

            Assert.AreEqual(count, list.Count);
        }

        [TestMethod()]
        public void ArcsWhereTest()
        {
            IEnumerator<Arc<string, int>> enumerator = _node.ArcsWhere(x => x.Value == 5).GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(5, enumerator.Current.Value);
        }

        [TestMethod()]
        public void FindArcTest()
        {
            MethodInfo method = _node.GetType().GetMethod("FindArc", 
                BindingFlags.NonPublic | BindingFlags.Instance);
            object arc = method.Invoke(_node, new object[] { _nodes[12] });
            int value = (int)arc.GetType().GetProperty("Value").GetValue(arc);
            Assert.AreEqual(12, value);
        }
    }
}
