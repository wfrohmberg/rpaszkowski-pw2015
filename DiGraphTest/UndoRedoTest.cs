﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DiGraphEditor.Commands;
using System.Collections.Generic;

namespace DiGraphTest
{
    [TestClass]
    public class UndoRedoTest
    {
        private class SimpleCommand : IReversibleCommand
        {
            private int number;
            private List<int> result;

            public SimpleCommand(int num, List<int> res)
            {
                number = num;
                result = res;
            }

            public void Execute(object parameter)
            {
                Console.WriteLine("Execute " + number);
                result.Add(number);
            }

            public void UnExecute()
            {
                Console.WriteLine("UnExecute: " + number);
                result.Add(number);
            }

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public IReversibleCommand Clone()
            {
                return null;
            }
        }

        private UndoRedo undoRedo;
        private List<int> result;

        [TestInitialize]
        public void setup()
        {
            result = new List<int>();
            undoRedo = UndoRedo.GetInstance();
            undoRedo.SetHistorySize(8);
            for (int i = 0; i < 8; i++)
            {
                SimpleCommand simCom = new SimpleCommand(i, result);
                undoRedo.AddCommand(simCom);
            }
        }

        [TestCleanup]
        public void cleanup()
        {
            undoRedo.ClearHistory();
            undoRedo = null;
        }

        [TestMethod]
        public void UndoTest()
        {
            undoRedo.Undo();
            undoRedo.Undo();
            undoRedo.Undo();

            CollectionAssert.AreEqual(new List<int> { 7, 6, 5 }, result);
        }

        [TestMethod]
        public void RedoTest()
        {
            undoRedo.Undo();
            undoRedo.Undo();
            undoRedo.Redo();

            CollectionAssert.AreEqual(new List<int> {7, 6, 6}, result);
        }

        [TestMethod]
        public void MaxUndoTest()
        {
            SimpleCommand simCom = new SimpleCommand(8, result);
            undoRedo.AddCommand(simCom);

            for (int i = 0; i < 12; i++)
            {
                undoRedo.Undo();
            }

            CollectionAssert.AreEqual(new List<int> { 8, 7, 6, 5, 4, 3, 2, 1 }, result);
        }

        [TestMethod]
        public void MaxRedoTest()
        {
            undoRedo.Redo();

            Assert.IsTrue(result.Count == 0);
        }
    }
}
