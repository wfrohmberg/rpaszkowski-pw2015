﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graph;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace DiGraphTest
{
    [TestClass]
    public class DiGraphUnitTest
    {
        private DiGraph<string, int> graph;

        [TestInitialize]
        public void Setup()
        {
            graph = new DiGraph<string, int>();
            graph[0] = "Zero";
            graph[1] = "One";
            graph[2] = "Two";
            graph[3] = "Three";

            graph[1, 0] = 5;
            graph[3, 2] = 2;
            graph[0, 2] = 5;
            graph[2, 3] = 32;
            graph[0, 1] = 6;
            graph[2, 0] = 12;
            graph[2, 1] = 43;
        }

        [TestMethod]
        public void NodesTest()
        {
            Assert.AreEqual("Two", graph[2].Value);
        }

        [TestMethod]
        public void SortArcsTest()
        {
            graph.Rebuild();

            Assert.AreEqual(32, graph[2, 3].Value);
        }

        [TestMethod]
        public void ModifyNodeTest()
        {
            graph[0] = "Null";

            Assert.AreEqual("Null", graph[0].Value);
        }

        [TestMethod]
        public void ModifyArcTest()
        {
            graph[3, 2] = 8;

            Assert.AreEqual(8, graph[3, 2].Value);
        }

        [TestMethod]
        public void NodesWhereTest()
        {
            Assert.AreEqual(graph[3], graph.NodesWhere(x => x.Value == "Three").First());
        }

        [TestMethod]
        public void NodesWhereNotExistsTest()
        {
            Assert.AreEqual(0, graph.NodesWhere(x => x.Value == "Five").Count());
        }

        [TestMethod]
        public void NotExistsNodeTest()
        {
            Assert.IsNull(graph[5]);
        }

        [TestMethod]
        public void NotExistsArcTest()
        {
            Assert.IsNull(graph[1, 2]);
        }

        [TestMethod]
        public void ArcsWhereTest()
        {
            Assert.AreEqual(2, graph.ArcsWhere(x => x.Value == 5).Count());
        }
    }
}
