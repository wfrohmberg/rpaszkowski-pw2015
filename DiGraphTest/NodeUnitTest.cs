﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graph;
using System.Collections.Generic;

namespace DiGraphTest
{
    [TestClass]
    public class NodeUnitTest
    {
        private Node<string, int> node;
        private List<Node<string, int>> nodes;

        [TestInitialize]
        public void Init()
        {
            nodes = new List<Node<string, int>>();
            node = new Node<string, int>("Node 0", 0);
            nodes.Add(node);

            Random rand = new Random();
            for (int i = 1; i < 20; i++)
            {
                Node<string, int> temp = new Node<string, int>("Node " + i, i);
                nodes.Add(temp);
            }

            for (int i = 19; i >= 1; i--)
            {
                node[nodes[i]] = i;
            }
        }

        [TestMethod]
        public void ChangeArc()
        {
            Random rand = new Random();
            int cur = node[nodes[5]].Value;
            int temp = rand.Next();
            while (temp == cur) temp = rand.Next(0);

            Console.WriteLine(node[nodes[5]].Value);

            node[nodes[5]] = new Arc<string, int>(temp);

            Console.WriteLine(node[nodes[5]].Value);

            Assert.IsTrue(node[nodes[5]].Value == temp && node[nodes[5]].Value != cur);
        }

        [TestMethod]
        public void FindNotJoinedNode()
        {
            Assert.IsNull(node[new Node<string, int>("I don't know you!", 20)]);
        }

        [TestMethod]
        public void SortArcsInNode()
        {
            List<int> list = new List<int>();
            foreach (Arc<string, int> arc in node)
            {
                Console.Write(arc.Value + " ");
                list.Add(arc.Value);
            }
            
            node.Sort();
            list.Sort();

            IEnumerator<Arc<string, int>> enumerator = node.GetEnumerator();
            Console.WriteLine(" ");

            int count = 0;

            while (enumerator.MoveNext())
            {
                Console.Write(enumerator.Current.Value + " ");
                Assert.AreEqual(enumerator.Current.Value, list[count]);
                count++;
            }

            Assert.AreEqual(count, list.Count);
        }

        [TestMethod]
        public void ArcsWhere()
        {
            IEnumerator<Arc<string, int>> enumerator = node.ArcsWhere(x => x.Value == 5).GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(5, enumerator.Current.Value);
        }
    }
}
