﻿using System.Linq;
using DiGraph;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiGraphTest
{
    [TestClass()]
    public class DiGraphUnitTest
    {
        private DiGraph<string, int> _graph;

        [TestInitialize()]
        public void Setup()
        {
            _graph = new DiGraph<string, int>();
            _graph[0] = "Zero";
            _graph[1] = "One";
            _graph[2] = "Two";
            _graph[3] = "Three";

            _graph[1, 0] = 5;
            _graph[3, 2] = 2;
            _graph[0, 2] = 5;
            _graph[2, 3] = 32;
            _graph[0, 1] = 6;
            _graph[2, 0] = 12;
            _graph[2, 1] = 43;
        }

        [TestMethod()]
        public void NodesTest()
        {
            Assert.AreEqual("Two", _graph[2].Value);
        }

        [TestMethod()]
        public void SortArcsTest()
        {
            _graph.Rebuild();

            Assert.AreEqual(32, _graph[2, 3].Value);
        }

        [TestMethod()]
        public void ModifyNodeTest()
        {
            _graph[0] = "Null";

            Assert.AreEqual("Null", _graph[0].Value);
        }

        [TestMethod()]
        public void ModifyArcTest()
        {
            _graph[3, 2] = 8;

            Assert.AreEqual(8, _graph[3, 2].Value);
        }

        [TestMethod()]
        public void NodesWhereTest()
        {
            Assert.AreEqual(_graph[3], _graph.NodesWhere(x => x.Value == "Three").First());
        }

        [TestMethod()]
        public void NodesWhereNotExistsTest()
        {
            Assert.AreEqual(0, _graph.NodesWhere(x => x.Value == "Five").Count());
        }

        [TestMethod()]
        public void NotExistsNodeTest()
        {
            Assert.IsNull(_graph[5]);
        }

        [TestMethod()]
        public void NotExistsArcTest()
        {
            Assert.IsNull(_graph[1, 2]);
        }

        [TestMethod()]
        public void ArcsWhereTest()
        {
            Assert.AreEqual(2, _graph.ArcsWhere(x => x.Value == 5).Count());
        }

        [TestMethod()]
        public void RemoveNodeTest()
        {
            _graph.RemoveNode(_graph[1]);
            Assert.AreEqual("Two", _graph[1].Value);
        }

        [TestMethod()]
        public void InsertNodeTest()
        {
            Node<string, int> node = new Node<string, int>("Insert", 2);
            _graph.InsertNode(node);
            Assert.AreEqual("Insert", _graph[2].Value);
        }

        [TestMethod()]
        public void GetAllNodesTest()
        {
            Assert.AreEqual("Zero", _graph.GetAllNodes().First().Value);
        }

        [TestMethod()]
        public void GetAllArcsTest()
        {
            int value = (from arcs in _graph.GetAllArcs()
                        where arcs.From.Id == 2 && arcs.To.Id == 3
                        select arcs.Value).First();

            Assert.AreEqual(32, value);
        }

        [TestMethod()]
        public void ClearGraphTest()
        {
            _graph.Clear();
            Assert.IsNull(_graph[0]);
        }

        [TestMethod()]
        public void AddNodeTest()
        {
            Node<string, int> node = new Node<string, int>("Hello");
            _graph.AddNode(node);
            Assert.AreEqual("Hello", _graph.Nodes.Last().Value);
        }

        [TestMethod()]
        public void AddExistingNodeTest()
        {
            int nodesSize = _graph.Nodes.Count;
            _graph.AddNode(_graph[0]);
            Assert.AreEqual(_graph.Nodes.Count, nodesSize);
        }

        [TestMethod()]
        public void AddHighIdNodeTest()
        {
            Node<string, int> node = new Node<string, int>("Hello", 20);
            _graph.AddNode(node);
            Assert.AreEqual(_graph.Nodes.Count - 1, _graph.Nodes.Last().Id);
        }
    }
}
