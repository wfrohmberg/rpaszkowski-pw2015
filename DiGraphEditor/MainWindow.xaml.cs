﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Properties;
using DiGraphEditor.ViewModel;

namespace DiGraphEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly MainViewModel _viewModel;

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Settings.Default["SaveFile"] = "0";
            MenuAdd.CommandParameter = GraphGrid;

            _viewModel = new MainViewModel(GraphGrid, Details);
            DataContext = _viewModel;
        }

        /// <summary>
        /// Mouse move handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse arguments</param>
        private void OnMouseMove(dynamic sender, MouseEventArgs e)
        {
            MousePosition.Text = "Position: <" + e.GetPosition(sender).ToString() + ">";
        }

        /// <summary>
        /// Closing handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Cancel arguments</param>
        private void DiGraphEditorClosing(object sender, CancelEventArgs e)
        {
            SettingsWindow toDelete = FindResource("SettingsWindow") as SettingsWindow;
            if (toDelete != null)
                toDelete.Kill();
        }

        /// <summary>
        /// Mouse down handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse button arguments</param>
        private void OnMouseDown(dynamic sender, MouseButtonEventArgs e)
        {
            _viewModel.OnMouseDown(sender, e);

            if (Keyboard.FocusedElement.GetType() != typeof(TextBox))
                return;

            IInputElement element = Keyboard.FocusedElement;
            IInputElement senderElement = sender as IInputElement;
            Keyboard.ClearFocus();
            element.Focusable = false;
            if (senderElement != null)
            {
                senderElement.Focusable = true;
                Keyboard.Focus(senderElement);
            }
            element.Focusable = true;
        }
    }
}
