﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using DiGraphEditor.Properties;
using DiGraphEditor.Reflection;
using DiGraphEditor.Utils;

namespace DiGraphEditor.Packer
{
    /// <summary>
    /// Class to serialize and deserialize graph
    /// </summary>
    public class XmlPacker : IPacker
    {
        /// <summary>
        /// Serializes the graph
        /// </summary>
        /// <typeparam name="T">Type of the graph</typeparam>
        /// <param name="toSerialize">The graph to serialize</param>
        /// <returns>Serialized graph as a string containing xml</returns>
        public string Serialize<T>(T toSerialize)
        {
            using(MemoryStream memoryStream = new MemoryStream())
            using(StreamReader reader = new StreamReader(memoryStream)) {
                Type graph = Util.GetInstance().GetGenericGraphType();
                Type node = Util.GetInstance().GetGenericNodeType();
                Type arc = Util.GetInstance().GetGenericArcType();
                Type[] known = { graph, node, arc };

                DataContractSerializer serializer = new DataContractSerializer(typeof(T), known);
                XmlWriterSettings settings = new XmlWriterSettings { Indent = true };

                using (XmlWriter writer = XmlWriter.Create(memoryStream, settings))
                {
                    serializer.WriteObject(writer, toSerialize);
                }

                memoryStream.Position = 0;
                return reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Deserializes a graph
        /// </summary>
        /// <typeparam name="T">Type of a graph</typeparam>
        /// <param name="content">String containing xml describing a graph</param>
        /// <returns>New graph</returns>
        public T Deserialize<T>(string content)
        {
            using (Stream stream = new MemoryStream())
            {
                byte[] data = Encoding.UTF8.GetBytes(content);
                stream.Write(data, 0, data.Length);
                stream.Position = 0;

                Type graph = Util.GetInstance().GetGenericGraphType();
                Type node = Util.GetInstance().GetGenericNodeType();
                Type arc = Util.GetInstance().GetGenericArcType();
                Type[] known = { graph, node, arc };
                DataContractSerializer deserializer = new DataContractSerializer(typeof(T), known);

                return (T) deserializer.ReadObject(stream);
            }
        }

        /// <summary>
        /// Method to pack graph to a zip archive
        /// </summary>
        /// <param name="path">Path of the zip archive</param>
        /// <param name="content">Content to pack</param>
        public void Pack(string path, string content)
        {
            using(MemoryStream memoryStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    PackXml(archive, content);
                    PackType(archive);
                    PackAssemblies(archive);
                }

                using (FileStream fileStream = new FileStream(path, FileMode.Create))
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    memoryStream.CopyTo(fileStream);
                }
            }
        }

        /// <summary>
        /// Packs xml file to a zip archive
        /// </summary>
        /// <param name="archive">A zip archive to which pack the xml</param>
        /// <param name="content">Xml string</param>
        private void PackXml(ZipArchive archive, string content)
        {
            ZipArchiveEntry xml = archive.CreateEntry("graph.xml");

            using (Stream entryStream = xml.Open())
            using (StreamWriter sw = new StreamWriter(entryStream))
            {
                sw.Write(content);
            }
        }

        /// <summary>
        /// Packs types of the graph, so during deserialization we can get the type
        /// </summary>
        /// <param name="archive">A zip archive to which pack the types</param>
        private void PackType(ZipArchive archive)
        {
            ZipArchiveEntry typeEntry = archive.CreateEntry("graph.type");

            using (Stream entryStream = typeEntry.Open())
            using (StreamWriter sw = new StreamWriter(entryStream))
            {
                string nodeType = Settings.Default["NodeType"] as string ?? "int";
                string arcType = Settings.Default["ArcType"] as string ?? "int";
                sw.WriteLine("NodeType: " + nodeType);
                sw.WriteLine("ArcType: " + arcType);
            }
        }

        /// <summary>
        /// Packs assemblies to a zip archive
        /// </summary>
        /// <param name="archive">A zip archive to which pack the assemblies</param>
        private void PackAssemblies(ZipArchive archive)
        {
            AssemblyManager manager = AssemblyManager.GetInstance();
            foreach (Assembly assembly in manager.GetAllAssemblies())
            {
                FileInfo file = new FileInfo(assembly.Location);
                using (FileStream fileStream = file.OpenRead())
                {
                    ZipArchiveEntry entry = archive.CreateEntry(file.Name);

                    using (Stream entryStream = entry.Open())
                    {
                        fileStream.CopyTo(entryStream);
                    }
                }
            }
        }

        /// <summary>
        /// Unpacks a graph from a zip archive
        /// </summary>
        /// <param name="path">Path to a zip archive containing graph and assemblies</param>
        /// <returns>Xml string describing a graph</returns>
        public string Unpack(string path)
        {
            string xml = UnpackXml(path);
            string types = UnpackType(path);
            AddTypes(types);

            UnpackAssemblies(path);
            return xml;
        }

        /// <summary>
        /// Unpacks an xml file from a zip archive
        /// </summary>
        /// <param name="path">Path to a zip archive containing xml file</param>
        /// <returns>Xml string</returns>
        private string UnpackXml(string path)
        {
            MemoryStream data = new MemoryStream();
            string xml;
            using (ZipArchive archive = ZipFile.OpenRead(path))
            {
                ZipArchiveEntry entry = archive.GetEntry("graph.xml") 
                    ?? (from entries in archive.Entries
                    where entries.Name.EndsWith(".xml")
                    select entries).First();

                if (entry == null)
                    return null;
                else
                {
                    entry.Open().CopyTo(data);
                    data.Position = 0;
                    StreamReader sr = new StreamReader(data);
                    xml = sr.ReadToEnd();
                    sr.Close();
                }
            }
            return xml;
        }

        /// <summary>
        /// Unpacks type file containing graph types
        /// </summary>
        /// <param name="path">Path to a zip archive containing graph types file</param>
        /// <returns>Types of graph as a string</returns>
        private string UnpackType(string path)
        {
            MemoryStream data = new MemoryStream();
            string types;
            using (ZipArchive archive = ZipFile.OpenRead(path))
            {
                ZipArchiveEntry entry = archive.GetEntry("graph.type") 
                    ?? (from entries in archive.Entries
                    where entries.Name.EndsWith("type")
                    select entries).First();

                if (entry == null)
                    return null;
                else
                {
                    entry.Open().CopyTo(data);
                    data.Position = 0;
                    StreamReader sr = new StreamReader(data);
                    types = sr.ReadToEnd();
                    sr.Close();
                }
            }
            return types;
        }

        /// <summary>
        /// Adds types to the application
        /// </summary>
        /// <param name="types">Types string</param>
        private void AddTypes(string types)
        {
            string nodeType = types.Split('\n')[0].Split(':')[1].Trim();
            string arcType = types.Split('\n')[1].Split(':')[1].Trim();

            if (!String.IsNullOrEmpty(nodeType))
                Settings.Default["NodeType"] = nodeType;
            if (!String.IsNullOrEmpty(arcType))
                Settings.Default["ArcType"] = arcType;
        }

        /// <summary>
        /// Unpacks assemblies
        /// </summary>
        /// <param name="path">Path to a zip archive containing assemblies</param>
        private void UnpackAssemblies(string path)
        {
            string unpackTo = Directory.GetCurrentDirectory() + "//assemblies";

            using (ZipArchive archive = ZipFile.OpenRead(path))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    if (entry.Name.EndsWith(".dll") || entry.Name.EndsWith(".exe"))
                    {
                        if (!Directory.Exists(unpackTo))
                            Directory.CreateDirectory(unpackTo);

                        string unpackFile = unpackTo + "//" + entry.Name;
                        if (!File.Exists(unpackFile))
                            entry.ExtractToFile(unpackFile);

                        AssemblyManager.GetInstance().AddAssembly(unpackFile);
                    }
                }
            }
        }
    }
}
