﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DiGraphEditor.Packer
{
    public class GraphResolver : DataContractResolver 
    {
        private Dictionary<string, XmlDictionaryString> dictionary = new Dictionary<string, XmlDictionaryString>();
        Assembly assembly;

        public GraphResolver(Assembly assembly)
        {
            this.assembly = assembly;
        }

        public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
        {
            XmlDictionaryString tName;
            XmlDictionaryString tNamespace;
            if (dictionary.TryGetValue(typeName, out tName) && dictionary.TryGetValue(typeNamespace, out tNamespace))
            {
                return this.assembly.GetType(tNamespace.Value + "." + tName.Value);
            }
            else
            {
                return null;
            }
        }

        public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out System.Xml.XmlDictionaryString typeName, out System.Xml.XmlDictionaryString typeNamespace)
        {
            DataContractAttribute attribute = (DataContractAttribute) type.GetCustomAttribute(typeof(DataContractAttribute));
            string name = attribute.Name;
            string namesp = attribute.Namespace;

            typeName = new XmlDictionaryString(XmlDictionary.Empty, name, 0);
            typeNamespace = new XmlDictionaryString(XmlDictionary.Empty, namesp, 0);
            if (!dictionary.ContainsKey(name))
            {
                dictionary.Add(name, typeName);
            }
            if (!dictionary.ContainsKey(namesp))
            {
                dictionary.Add(namesp, typeNamespace);
            }
            return true;
        }
    }
}
