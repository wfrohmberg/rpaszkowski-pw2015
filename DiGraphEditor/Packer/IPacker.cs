﻿namespace DiGraphEditor.Packer
{
    /// <summary>
    /// Interface for packing
    /// </summary>
    public interface IPacker
    {
        /// <summary>
        /// Serializes an element
        /// </summary>
        /// <typeparam name="T">Type of the object to serialize</typeparam>
        /// <param name="toSerialize">Object to serialize</param>
        /// <returns>String containing serialized object</returns>
        string Serialize<T>(T toSerialize);
        
        /// <summary>
        /// Deserializes an element
        /// </summary>
        /// <typeparam name="T">Type of the object to deserialize</typeparam>
        /// <param name="content">String containing serialized object</param>
        /// <returns>Deserialized object</returns>
        T Deserialize<T>(string content);

        /// <summary>
        /// Packs content
        /// </summary>
        /// <param name="path">Path to pack file</param>
        /// <param name="content">Content to pack</param>
        void Pack(string path, string content);

        /// <summary>
        /// Unpacks file
        /// </summary>
        /// <param name="path">Path to file to unpack</param>
        /// <returns>Unpacked file as a string</returns>
        string Unpack(string path);
    }
}
