﻿using System;
using System.CodeDom;
using System.Reflection;
using DiGraph;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Properties;
using Microsoft.CSharp;

namespace DiGraphEditor.Utils
{
    /// <summary>
    /// Utilities for graphs
    /// Singleton for optimization purposes
    /// </summary>
    public class Util
    {
        private static Util _instance;
        private Type _nodeType;
        private Type _arcType;
        private string _nodeName;
        private string _arcName;

        private Util() { }

        /// <summary>
        /// Gets instance of the utilities
        /// </summary>
        /// <returns>Instance of utilities</returns>
        public static Util GetInstance()
        {
            return _instance ?? (_instance = new Util());
        }

        /// <summary>
        /// Gets generic graph type
        /// </summary>
        /// <param name="node">Type of a node</param>
        /// <param name="arc">Type of an arc</param>
        /// <returns>Generic graph type</returns>
        public Type GetGenericGraphType(string node = null, string arc = null)
        {
            return typeof(GraphEntity<,>).MakeGenericType(GetGenericType(node, arc));
        }

        /// <summary>
        /// Gets generic base arc type
        /// </summary>
        /// <param name="node">Type of a node</param>
        /// <param name="arc">Type of an arc</param>
        /// <returns>Generic base arc type</returns>
        public Type GetGenericBaseArcType(string node = null, string arc = null)
        {
            return typeof(Arc<,>).MakeGenericType(GetGenericType(node, arc));
        }

        /// <summary>
        /// Gets generic arc type
        /// </summary>
        /// <param name="node">Type of a node</param>
        /// <param name="arc">Type of an arc</param>
        /// <returns>Generic arc type</returns>
        public Type GetGenericArcType(string node = null, string arc = null)
        {
            return typeof(ArcEntity<,>).MakeGenericType(GetGenericType(node, arc));
        }

        /// <summary>
        /// Gets generic base node type
        /// </summary>
        /// <param name="node">Type of a node</param>
        /// <param name="arc">Type of an arc</param>
        /// <returns>Generic base node type</returns>
        public Type GetGenericBaseNodeType(string node = null, string arc = null)
        {
            return typeof(Node<,>).MakeGenericType(GetGenericType(node, arc));
        }

        /// <summary>
        /// Gets generic node type
        /// </summary>
        /// <param name="node">Type of a node</param>
        /// <param name="arc">Type of an arc</param>
        /// <returns>Generic node type</returns>
        public Type GetGenericNodeType(string node = null, string arc = null)
        {
            return typeof(NodeEntity<,>).MakeGenericType(GetGenericType(node, arc));
        }

        /// <summary>
        /// Gets generic type
        /// </summary>
        /// <param name="node">Type of a node</param>
        /// <param name="arc">Type of an arc</param>
        /// <returns>Generic type</returns>
        private Type[] GetGenericType(string node = null, string arc = null)
        {
            Type[] type = new Type[2];
            string nType;
            string aType;

            if (node != null)
                nType = node;
            else
                nType = (string)Settings.Default["NodeType"];

            if (arc != null)
                aType = arc;
            else
                aType = (string)Settings.Default["ArcType"];

            if (_nodeName == nType && _nodeType != null)
                type[0] = _nodeType;
            else
            {
                type[0] = Type.GetType(nType);
                _nodeName = nType;
            }
            if (_arcName == aType && _arcType != null)
                type[1] = _arcType;
            else
            {
                type[1] = Type.GetType(aType);
                _arcName = aType;
            }

            if (type[0] == null)
                type[0] = GetPrimitiveType(nType);
            if (type[1] == null)
                type[1] = GetPrimitiveType(aType);

            if (_nodeType != null && _nodeType != type[0])
                _nodeType = type[0];
            if (type.Length > 1 && _arcType != type[1])
                _arcType = type[1];

            Settings.Default["NodeType"] = nType;
            Settings.Default["ArcType"] = aType;

            return type;
        }

        /// <summary>
        /// Gets primitive type from string
        /// </summary>
        /// <param name="primitive">String representing a primitive (ex: int)</param>
        /// <returns></returns>
        private Type GetPrimitiveType(string primitive)
        {
            Assembly mscorlib = Assembly.GetAssembly(typeof(int));
            Type res = typeof(int);

            using (CSharpCodeProvider provider = new CSharpCodeProvider())
            {
                foreach(TypeInfo type in mscorlib.DefinedTypes)
                {
                    CodeTypeReference typeRef = new CodeTypeReference(type);
                    string typeName = provider.GetTypeOutput(typeRef);

                    if (typeName.Equals(primitive))
                    {
                        res = Type.GetType(type.FullName);
                        break;
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Checks if a given type is primitive
        /// </summary>
        /// <param name="info">Property info</param>
        /// <returns>True if type is primitive</returns>
        public bool CheckIfPrimitive(PropertyInfo info)
        {
            Type type = info.PropertyType;
            if (type.Namespace == "System" || type.IsPrimitive)
                return true;
            else
                return false;
        }
    }
}
