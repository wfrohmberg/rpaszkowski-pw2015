﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace DiGraphEditor.Utils
{
    /// <summary>
    /// Creates lambda expressions from string
    /// </summary>
    public static class LambdaCreator
    {
        /// <summary>
        /// Creates lambda expression from string (It is really ugly quickly written class. It should use the Interpreter design pattern)
        /// </summary>
        /// <param name="expType">Type of expression</param>
        /// <param name="sentence">string representing the lambda expression</param>
        /// <param name="parameters">parameters for the lambda expression (like constants)</param>
        /// <returns>Created lambda expression</returns>
        public static LambdaExpression SimpleCreate(Type expType, string sentence, object[] parameters = null)
        {
            List<string> words = new List<string>(sentence.Split(' '));
            ParameterExpression predParam = Expression.Parameter(expType, words[0]);
            string predString = words[0];
            words.RemoveRange(0, 2);

            Expression left = null;
            string signExp = null;
            string logicExp = null;
            Expression leftLogic = null;
            int i = 0;

            foreach (string word in words)
            {
                Expression temp = null;
                switch(word[0])
                {
                    case 's':
                        signExp = word.Substring(1);
                        break;
                    case 'c':
                        Debug.Assert(parameters != null, "parameters != null");
                        temp = Expression.Constant(parameters[i], parameters[i].GetType());
                        i++;
                        break;
                    case 'e':
                        temp = predParam;
                        string tWord = word.Substring(1);
                        tWord = tWord.Remove(tWord.IndexOf(predString, StringComparison.Ordinal), predString.Length + 1);
                        temp = tWord.Split('.').Aggregate(temp, Expression.Property);
                        break;
                    case 'l':
                        logicExp = word.Substring(1);
                        break;
                }

                if (temp == null)
                    continue;

                if (left == null)
                {
                    left = temp;
                }
                else
                {
                    Expression right = temp;
                    switch(signExp)
                    {
                        case "==":
                            temp = Expression.Equal(left, right);
                            break;
                        case "<=":
                            temp = Expression.LessThanOrEqual(left, right);
                            break;
                        case ">=":
                            temp = Expression.GreaterThanOrEqual(left, right);
                            break;
                        case "!=":
                            temp = Expression.NotEqual(left, right);
                            break;
                        case "<":
                            temp = Expression.LessThan(left, right);
                            break;
                        case ">":
                            temp = Expression.GreaterThan(left, right);
                            break;
                    }
                    signExp = null;
                    left = null;

                    if (logicExp == null)
                        leftLogic = temp;
                    else
                    {
                        var rightLogic = temp;
                        switch (logicExp)
                        {
                            case "and":
                                if (leftLogic != null)
                                    temp = Expression.And(leftLogic, rightLogic);
                                break;
                            case "or":
                                if (leftLogic != null)
                                    temp = Expression.Or(leftLogic, rightLogic);
                                break;
                        }

                        leftLogic = temp;
                    }
                }
            }

            Expression result = leftLogic;

            Type predType = typeof(Predicate<>).MakeGenericType(expType);
            if (result != null) return Expression.Lambda(predType, result, predParam);
            return null;
        }
    }
}
