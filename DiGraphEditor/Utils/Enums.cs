﻿namespace DiGraphEditor.Utils
{
    /// <summary>
    /// Layout strategies
    /// </summary>
    public enum LayoutStrategies { Circle, Tree, Hierarchical };

    /// <summary>
    /// Change options
    /// </summary>
    public enum ChangeOptions { Node, Arc };
}
