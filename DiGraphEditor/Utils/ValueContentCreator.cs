﻿using System;
using System.Reflection;
using System.Runtime.Remoting;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using DiGraphEditor.Converters;

namespace DiGraphEditor.Utils
{
    /// <summary>
    /// Creates in code WPF elements
    /// </summary>
    public static class ValueContentCreator
    {
        /// <summary>
        /// Creates panel representing position
        /// </summary>
        /// <param name="panelName">Name of a panel</param>
        /// <param name="posText">Label of a text box</param>
        /// <param name="value">Position of an elemtn</param>
        /// <returns></returns>
        public static Panel CreatePositionPanel(string panelName, string posText, dynamic value)
        {
            if (value == null || value.GetType().GetProperty("Pos") == null)
                return null;

            Grid panel = CreateValueGrid();
            panel.Name = panelName;

            TextBlock positionName = new TextBlock() { Text = posText, Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255)) };
            positionName.SetValue(Grid.ColumnProperty, 0);

            TextBlock positionValue = new TextBlock()
            {
                Name = "PositionValue",
                Text = "<" + value.Pos + ">",
                Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255))
            };
            positionValue.SetValue(Grid.ColumnProperty, 1);

            panel.Children.Add(positionName);
            panel.Children.Add(positionValue);

            return panel;
        }

        /// <summary>
        /// Recursive method getting all nested values until it reaches maximal depth or there are no more nested values
        /// </summary>
        /// <param name="parent">Parent panel to which we will add controls</param>
        /// <param name="obj">Object which we will check for nested values</param>
        /// <param name="value">Property info of the object</param>
        /// <param name="depth">Current depth</param>
        public static void GetNestedValues(Panel parent, dynamic obj, PropertyInfo value, int depth) 
        {
            if (depth > 0)
            {
                depth--;
                if (Util.GetInstance().CheckIfPrimitive(value))
                {
                    CreateValuePanel(parent, obj, value);
                }
                else
                {
                    if (value.GetValue(obj) == null)
                    {
                        string assemblyName = value.PropertyType.Assembly.FullName;
                        string propertyName = value.PropertyType.FullName;
                        ObjectHandle handle = Activator.CreateInstance(assemblyName, propertyName);
                        value.SetValue(obj, handle.Unwrap());
                    }

                    Panel panel = new StackPanel();
                    foreach (PropertyInfo info in value.PropertyType.GetProperties())
                        GetNestedValues(panel, value.GetValue(obj), info, depth);

                    if (panel.Children.Count > 0)
                    {
                        Expander exp = CreateExpanderPanel(value.Name, panel);
                        parent.Children.Add(exp);
                    }
                }
            }
        }

        /// <summary>
        /// Creates expander panel
        /// </summary>
        /// <param name="name">A name of an expander panel</param>
        /// <param name="panel">Content of an expander panel</param>
        /// <returns>New expander panel</returns>
        private static Expander CreateExpanderPanel(String name, Panel panel)
        {
            Expander exp = new Expander
            {
                Header = name,
                Content = panel,
                IsExpanded = true
            };
            return exp;
        }

        /// <summary>
        /// Creates value panel
        /// </summary>
        /// <param name="parent">Panel to add this value panel to</param>
        /// <param name="obj">An object from which we will get value</param>
        /// <param name="value">Property info of an object</param>
        private static void CreateValuePanel(Panel parent, dynamic obj, PropertyInfo value)
        {
            Grid panel = CreateValueGrid();

            TextBlock label = new TextBlock() { Text = value.Name + ": ",
                Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255)),
                VerticalAlignment = VerticalAlignment.Center };
            label.SetValue(Grid.ColumnProperty, 0);

            panel.Children.Add(label);

            if (value.PropertyType == typeof(bool))
            {
                ComboBox valueBox = new ComboBox() { Background = new SolidColorBrush(Color.FromRgb(100, 100, 100)) };
                valueBox.SetValue(Grid.ColumnProperty, 1);
                ComboBoxItem trueValue = new ComboBoxItem {Content = true};
                ComboBoxItem falseValue = new ComboBoxItem {Content = false};
                valueBox.Items.Add(trueValue);
                valueBox.Items.Add(falseValue);
                valueBox.SetBinding(Selector.SelectedIndexProperty, new Binding(value.Name) { Source = obj, Converter = new BoolToIndexConverter() });

                panel.Children.Add(valueBox);
            } 
            else
            {
                TextBox valueBox = new TextBox() { Background = new SolidColorBrush(Color.FromRgb(100, 100, 100)) };
                valueBox.SetBinding(TextBox.TextProperty, new Binding(value.Name) { Source = obj });
                valueBox.SetValue(Grid.ColumnProperty, 1);
                panel.Children.Add(valueBox);
            } 

            parent.Children.Add(panel);
        }

        /// <summary>
        /// Creates delete button
        /// </summary>
        /// <param name="obj">Object to delete</param>
        /// <returns>New delete button</returns>
        public static Button CreateDeleteButton(dynamic obj)
        {
            Button delBtn = new Button();
            if (obj.GetType().Name.Contains("Node"))
                delBtn.Command = AppManager.GetInstance().DeleteNode;
            else if (obj.GetType().Name.Contains("Arc"))
                delBtn.Command = AppManager.GetInstance().DeleteArc;
            else
                return null;

            delBtn.CommandParameter = obj;
            delBtn.Content = "Delete";
            delBtn.FontSize = 14;
            delBtn.Margin = new Thickness(0, 5, 0, 5);

            return delBtn;
        }

        /// <summary>
        /// Creates value grid
        /// </summary>
        /// <returns>New value grid</returns>
        private static Grid CreateValueGrid()
        {
            Grid panel = new Grid();
            ColumnDefinition def1 = new ColumnDefinition {Width = new GridLength(1, GridUnitType.Star)};
            ColumnDefinition def2 = new ColumnDefinition {Width = new GridLength(2, GridUnitType.Star)};
            panel.ColumnDefinitions.Add(def1);
            panel.ColumnDefinitions.Add(def2);

            return panel;
        }
    }
}
