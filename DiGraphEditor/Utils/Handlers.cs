﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DiGraphEditor.Utils
{
    public static class Handlers
    {
        public static void AddHandler(object handlerObject, object eventObject, string evType, string evHandler)
        {
            EventInfo ev = eventObject.GetType().GetEvent(evType);
            Delegate aDelegate = CreateDelegate(ev, handlerObject, evHandler);

            MethodInfo addHandler = ev.GetAddMethod();
            object[] addHandlerArgs = { aDelegate };
            addHandler.Invoke(eventObject, addHandlerArgs);
        }

        public static void RemoveHandler(object handlerObject, object eventObject, string evType, string evHandler)
        {
            EventInfo ev = eventObject.GetType().GetEvent(evType);
            Delegate aDelegate = CreateDelegate(ev, handlerObject, evHandler);

            MethodInfo removeHandler = ev.GetRemoveMethod();
            object[] removeHandlerArgs = { aDelegate };
            removeHandler.Invoke(eventObject, removeHandlerArgs);
        }

        public static Delegate CreateDelegate(EventInfo ev, object handlerObject, string evHandler)
        {
            Type delegateType = ev.EventHandlerType;
            MethodInfo mdHandler = handlerObject.GetType().GetMethod(evHandler, BindingFlags.Instance | BindingFlags.NonPublic);
            Delegate aDelegate = Delegate.CreateDelegate(delegateType, handlerObject, mdHandler);
            return aDelegate;
        }
    }
}
