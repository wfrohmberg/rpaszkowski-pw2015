﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DiGraphEditor.GraphVisualizer
{
    public class GraphMemento
    {
        private Dictionary<int, Point> state;

        public GraphMemento()
        {
            state = new Dictionary<int, Point>();
        }

        public Dictionary<int, Point> GetState()
        {
            return state;
        }

        public void SetState(Dictionary<int, Point> aState)
        {
            state = aState;
        }

        public void AddToState(int id, Point point)
        {
            state.Add(id, point);
        }
    }
}
