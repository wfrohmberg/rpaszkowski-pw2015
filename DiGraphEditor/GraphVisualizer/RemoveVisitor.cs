﻿using System.Windows.Controls;

namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Visitor to remove graph elements from grid
    /// </summary>
    public class RemoveVisitor : IVisitor
    {
        private readonly Grid _grid;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid from which we remove graph elements</param>
        public RemoveVisitor(Grid aGrid)
        {
            _grid = aGrid;
        }

        /// <summary>
        /// Visits graph and removes its elements from a grid
        /// </summary>
        /// <param name="graph">A graph to remove</param>
        public void VisitGraph(dynamic graph)
        {
            foreach (dynamic node in graph.Nodes)
                VisitNode(node);

            foreach (dynamic arc in graph.GetAllArcs())
                VisitArc(arc);
        }

        /// <summary>
        /// Visits node and removes it from a grid
        /// </summary>
        /// <param name="node">A node to remove</param>
        public void VisitNode(dynamic node)
        {
            node.RemoveFromGrid(_grid);
            node.RemoveHandlers(_grid);
        }

        /// <summary>
        /// Visits arc and removes it from a grid
        /// </summary>
        /// <param name="arc">An arc to remove</param>
        public void VisitArc(dynamic arc)
        {
            arc.RemoveFromGrid(_grid);
            arc.RemoveHandlers(_grid);
        }
    }
}
