﻿using System.Windows.Input;

namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Delegate to handle the change of the entity
    /// </summary>
    /// <param name="sender">Changed entity</param>
    /// <param name="e">Mouse arguments</param>
    public delegate void EntityChangedEventHandler(dynamic sender, MouseEventArgs e);
}
