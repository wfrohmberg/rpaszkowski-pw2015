﻿namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Visitor interface for graphs
    /// </summary>
    public interface IVisitor
    {
        /// <summary>
        /// Visits a graph
        /// </summary>
        /// <param name="graph">A graph to visit</param>
        void VisitGraph(object graph);

        /// <summary>
        /// Visits a node
        /// </summary>
        /// <param name="node">A node to visit</param>
        void VisitNode(object node);

        /// <summary>
        /// Visits an arc
        /// </summary>
        /// <param name="arc">An arc to visit</param>
        void VisitArc(object arc);
    }
}
