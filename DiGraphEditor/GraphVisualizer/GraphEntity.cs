﻿using System.Runtime.Serialization;
using DiGraph;

namespace DiGraphEditor.GraphVisualizer
{
    [DataContract(Name = "GraphEntity", Namespace = "DiGraphEditor.GraphVisualizer")]
    public class GraphEntity<TNodeType, TArcType> : DiGraph<TNodeType, TArcType>
    {
        /// <summary>
        /// Adds a node to the graph
        /// </summary>
        /// <returns>Created node</returns>
        public override Node<TNodeType, TArcType> AddNode()
        {
            Node<TNodeType, TArcType> node = new NodeEntity<TNodeType, TArcType>(Nodes.Count);
            Nodes.Add(node);
            return node;
        }

        /// <summary>
        /// Removes a node from the graph
        /// </summary>
        /// <param name="node">Node to remove</param>
        public override void RemoveNode(Node<TNodeType, TArcType> node)
        {
            base.RemoveNode(node);

            foreach (var nodes in NodesWhere(x => x.Id >= node.Id))
            {
                var enNode = (NodeEntity<TNodeType, TArcType>) nodes;
                enNode.OnPropertyChanged("Id");
            }
        }

        /// <summary>
        /// Adds specified node with id to the graph 
        /// </summary>
        /// <param name="node">Node to add</param>
        public override void InsertNode(Node<TNodeType, TArcType> node)
        {
            base.InsertNode(node);

            foreach (var nodes in NodesWhere(x => x.Id >= node.Id))
            {
                var enNode = (NodeEntity<TNodeType, TArcType>) nodes;
                enNode.OnPropertyChanged("Id");
            }
        }

        /// <summary>
        /// Indexer for nodes
        /// </summary>
        /// <param name="index">A unique id of a node</param>
        /// <returns>A specified node</returns>
        new public Node<TNodeType, TArcType> this[int index]
        {
            get
            {
                return Nodes[index];
            }
            set
            {
                Node<TNodeType, TArcType> node = new NodeEntity<TNodeType, TArcType>(value.Value, index);
                Nodes[index] = node;
            }
        }
    }
}
