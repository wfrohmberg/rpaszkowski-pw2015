﻿using System.Collections.Generic;
using System.Windows;

namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Memento to store nodes positions
    /// </summary>
    public class NodesMemento
    {
        private Dictionary<int, Point> _state;

        /// <summary>
        /// Constructor
        /// </summary>
        public NodesMemento()
        {
            _state = new Dictionary<int, Point>();
        }

        /// <summary>
        /// Returns nodes state
        /// </summary>
        /// <returns>Nodes state</returns>
        public Dictionary<int, Point> GetState()
        {
            return _state;
        }

        /// <summary>
        /// Sets the state of nodes
        /// </summary>
        /// <param name="aState">State of nodes</param>
        public void SetState(Dictionary<int, Point> aState)
        {
            _state = aState;
        }

        /// <summary>
        /// Adds a node state
        /// </summary>
        /// <param name="id">Id of the node</param>
        /// <param name="point">Position of the node</param>
        public void AddToState(int id, Point point)
        {
            _state.Add(id, point);
        }
    }
}
