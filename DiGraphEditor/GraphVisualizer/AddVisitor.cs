﻿using System.Windows.Controls;

namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Visitor for adding elements to the grid
    /// </summary>
    public class AddVisitor : IVisitor
    {
        private readonly Grid _grid;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid to which we will add graph elements</param>
        public AddVisitor(Grid aGrid)
        {
            _grid = aGrid;
        }

        /// <summary>
        /// Visits graph and adds its elements to grid
        /// </summary>
        /// <param name="graph">A graph to visit</param>
        public void VisitGraph(dynamic graph)
        {
            foreach (dynamic node in graph.Nodes)
                VisitNode(node);

            foreach (dynamic arc in graph.GetAllArcs())
                VisitArc(arc);

            AppManager.GetInstance().Graph = graph;
        }

        /// <summary>
        /// Visits node and adds it to grid
        /// </summary>
        /// <param name="node">A node to visit</param>
        public void VisitNode(dynamic node)
        {
            node.AddToGrid(_grid);
            node.AddHandlers(_grid);
        }

        /// <summary>
        /// Visits arc and adds it to grid
        /// </summary>
        /// <param name="arc">An arc to visit</param>
        public void VisitArc(dynamic arc)
        {
            arc.AddToGrid(_grid);
            arc.AddHandlers(_grid);
        }
    }
}
