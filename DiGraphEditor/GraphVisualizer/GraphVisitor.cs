﻿using DiGraphEditor.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DiGraphEditor.GraphVisualizer
{
    public class GraphVisitor : IVisitor
    {
        private Canvas canvas;

        public GraphVisitor(Canvas aCanvas)
        {
            canvas = aCanvas;
            canvas.Children.Clear();
        }

        public void VisitGraph(dynamic graph)
        {
            foreach (dynamic node in graph.GetAllNodes())
                VisitNode(node);

            foreach (dynamic arc in graph.GetAllArcs())
                VisitArc(arc);
        }

        public void VisitNode(dynamic node)
        {
            node.AddToCanvas(canvas);
        }

        public void VisitArc(dynamic arc)
        {
            arc.AddToCanvas(canvas);
        }
    }
}
