﻿using System;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using DiGraph;

namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Extension of arc class to handle things like graphics and arc updates
    /// </summary>
    /// <typeparam name="TNodeType"></typeparam>
    /// <typeparam name="TArcType"></typeparam>
    [DataContract(Name = "ArcEntity", Namespace = "DiGraphEditor.GraphVisualizer", IsReference = true)]
    public class ArcEntity<TNodeType, TArcType> : Arc<TNodeType, TArcType>
    {
        /// <summary>
        /// Visual representation of the arc
        /// </summary>
        [IgnoreDataMember]
        public Line Entity { get; set; }
        /// <summary>
        /// Visual representation of the arc
        /// </summary>
        [IgnoreDataMember]
        public Polygon Arrow { get; set; }

        [IgnoreDataMember]
        private const float Offset = 5;

        /// <summary>
        /// Event which will be run when arc value changes
        /// </summary>
        public event EntityChangedEventHandler ArcUpdate;

        /// <summary>
        /// Constructor
        /// </summary>
        public ArcEntity()
        {
            CreateEntity();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="start">Starting node of the arc</param>
        public ArcEntity(NodeEntity<TNodeType, TArcType> start)
        {
            From = start;
            CreateEntity();
        }

        /// <summary>
        /// Creates visual representation of the arc
        /// </summary>
        private void CreateEntity() 
        {
            NodeEntity<TNodeType, TArcType> fromNode = From as NodeEntity<TNodeType, TArcType>;
            if (fromNode == null)
                return;

            Point pos = fromNode.GetNodeCenter();

            Brush brush = new SolidColorBrush(Color.FromRgb(30, 30, 30));

            Entity = new Line
            {
                X1 = pos.X,
                Y1 = pos.Y,
                Stroke = brush,
                StrokeThickness = 4,
                StrokeEndLineCap = PenLineCap.Flat
            };
            Panel.SetZIndex(Entity, -1);

            NodeEntity<TNodeType, TArcType> toNode = To as NodeEntity<TNodeType, TArcType>;
            if (toNode != null)
                pos = toNode.GetNodeCenter();

            Entity.X2 = pos.X;
            Entity.Y2 = pos.Y;

            Arrow = new Polygon
            {
                Stroke = brush,
                Fill = brush
            };
            PointCollection points = new PointCollection {new Point(-4, -20), new Point(4, -20), new Point(0, 0)};
            Arrow.Points = points;
            Panel.SetZIndex(Arrow, -1);
        }

        /// <summary>
        /// Moves arc to given position
        /// </summary>
        /// <param name="pos">Position where to move the arc</param>
        public void MoveTo(Point pos)
        {
            NodeEntity<TNodeType, TArcType> node = To as NodeEntity<TNodeType, TArcType>;
            if (node != null) pos = node.GetNodeCenter();

            Entity.X2 = pos.X;
            Entity.Y2 = pos.Y;

            MoveArrow(pos);
            ApplyOffset();

            if (ArcUpdate != null)
                ArcUpdate(this, null);
        }

        /// <summary>
        /// Updates the arc state
        /// </summary>
        public void UpdateArc()
        {
            NodeEntity<TNodeType, TArcType> from = From as NodeEntity<TNodeType, TArcType>;
            if (@from != null)
            {
                Entity.X1 = @from.GetNodeCenter().X;
                Entity.Y1 = @from.GetNodeCenter().Y;
            }
            MoveTo(new Point());
        }

        /// <summary>
        /// Applies offset so two arcs don't overlap
        /// </summary>
        private void ApplyOffset()
        {
            if (To == null)
                return;
            
            Point start = new Point(Entity.X1, Entity.Y1);
            Point end = new Point(Entity.X2, Entity.Y2);
            double angle = Math.Atan2(end.X - start.X, end.Y - start.Y) * 180.0 / Math.PI;
            angle += 90.0;
            angle *= Math.PI / 180.0;

            Entity.X1 += Offset * Math.Sin(angle);
            Entity.Y1 += Offset * Math.Cos(angle);
            Entity.X2 += Offset * Math.Sin(angle);
            Entity.Y2 += Offset * Math.Cos(angle);

            Arrow.Margin = new Thickness(Entity.X2, Entity.Y2, 0, 0);
        }

        /// <summary>
        /// Moves arrow so it is at the and of the line pointing in the right direction
        /// </summary>
        /// <param name="pos">Position of the end of the arc</param>
        private void MoveArrow(Point pos)
        {
            Arrow.Margin = new Thickness(pos.X, pos.Y, 0, 0);

            NodeEntity<TNodeType, TArcType> node = From as NodeEntity<TNodeType, TArcType>;

            if (node != null)
            {
                double angle = -Math.Atan2(pos.X - node.GetNodeCenter().X, pos.Y - node.GetNodeCenter().Y) * 180.0 / Math.PI;
                RotateTransform rotate = new RotateTransform(angle);
                Arrow.RenderTransform = rotate;
            }
        }

        /// <summary>
        /// Sets the end of the arc
        /// </summary>
        /// <param name="node">Ending node of the arc</param>
        public void SetEnd(NodeEntity<TNodeType, TArcType> node)
        {
            To = node;
            MoveTo(node.Pos);
        }

        /// <summary>
        /// Mouse move handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse arguments</param>
        public void OnMouseMove(object sender, MouseEventArgs e)
        {
            Grid grid = sender as Grid;
            if (grid == null) return;

            Point pos = e.GetPosition(grid);
            MoveTo(pos);

            if (ArcUpdate != null)
                ArcUpdate(this, e);
        }

        /// <summary>
        /// Mouse down handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse button arguments</param>
        public void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!Entity.IsMouseOver)
            {
                Brush brush = new SolidColorBrush(Color.FromRgb(30, 30, 30));
                Entity.Stroke = brush;
                Arrow.Stroke = brush;
            }
            else
            {
                Brush brush = new SolidColorBrush(Color.FromRgb(160, 30, 30));
                Entity.Stroke = brush;
                Arrow.Stroke = brush;
                if (ArcUpdate != null)
                    ArcUpdate(this, e);
            }
        }

        /// <summary>
        /// Adds its handlers to a grid
        /// </summary>
        /// <param name="grid">A grid which will feed the events</param>
        public void AddHandlers(Grid grid)
        {
            grid.MouseDown += OnMouseDown;
        }

        /// <summary>
        /// Removes its handlers from a grid
        /// </summary>
        /// <param name="grid">A grid from which we remove handlers</param>
        public void RemoveHandlers(Grid grid)
        {
            grid.MouseDown -= OnMouseDown;
        }

        /// <summary>
        /// Adds arc to a grid
        /// </summary>
        /// <param name="grid">A grid to add arc to</param>
        public void AddToGrid(Grid grid)
        {
            grid.Children.Add(Entity);
            grid.Children.Add(Arrow);
        }

        /// <summary>
        /// Removes arc from a grid
        /// </summary>
        /// <param name="grid">A grid from which to remove arc</param>
        public void RemoveFromGrid(Grid grid)
        {
            grid.Children.Remove(Entity);
            grid.Children.Remove(Arrow);
        }
    }
}
