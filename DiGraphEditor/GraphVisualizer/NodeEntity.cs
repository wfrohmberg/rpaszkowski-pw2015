﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using DiGraph;
using DiGraphEditor.Commands;

namespace DiGraphEditor.GraphVisualizer
{
    /// <summary>
    /// Extension of node class to handle things like graphics and node updates
    /// </summary>
    /// <typeparam name="TNodeType"></typeparam>
    /// <typeparam name="TArcType"></typeparam>
    [DataContract(Name = "NodeEntity", Namespace = "DiGraphEditor.GraphVisualizer", IsReference = true)]
    public class NodeEntity<TNodeType, TArcType> : Node<TNodeType, TArcType>, INotifyPropertyChanged
    {
        /// <summary>
        /// Position of the node
        /// </summary>
        [DataMember]
        public Point Pos { get; set; }

        [IgnoreDataMember]
        private bool _dragged;
        [IgnoreDataMember]
        private bool _selected;
        [IgnoreDataMember]
        private MoveNodeCommand _moveCommand;

        /// <summary>
        /// Add arc command
        /// </summary>
        [IgnoreDataMember]
        public AddArcCommand ArcCommand { get; set; }

        /// <summary>
        /// Visual representation of the arc
        /// </summary>
        [IgnoreDataMember]
        public Border Entity { get; set; }

        /// <summary>
        /// Event which notifies about the node update
        /// </summary>
        public event EntityChangedEventHandler NodeUpdate;

        /// <summary>
        /// Event which notifies about chnage of a property of the node
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Id of the node (optional)</param>
        public NodeEntity(int id = 0)
            : base(id)
        {
            CreateEntity(new Point(0, 0));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">Value of the node</param>
        /// <param name="id">Id of the node (optional)</param>
        public NodeEntity(TNodeType value, int id = 0)
            : this(id)
        {
            Value = value;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">Value fo the node</param>
        /// <param name="id">Id of the node</param>
        /// <param name="pos">Position of the node</param>
        public NodeEntity(TNodeType value, int id, Point pos) 
            : base(value, id)
        {
            CreateEntity(pos);
        }

        /// <summary>
        /// Creates entity at the given position
        /// </summary>
        /// <param name="pos">Where to create the entity</param>
        private void CreateEntity(Point pos)
        {
            Pos = pos;
            Entity = new Border
            {
                Height = 20,
                Width = 20,
                BorderThickness = new Thickness(1),
                CornerRadius = new CornerRadius(90),
                Background = new SolidColorBrush(Color.FromRgb(200, 200, 200)),
                BorderBrush = new SolidColorBrush(Color.FromRgb(25, 25, 25)),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top
            };

            TextBlock text = new TextBlock();
            text.SetBinding(TextBlock.TextProperty, new Binding("Id") { Source = this });
            text.Foreground = new SolidColorBrush(Color.FromRgb(200, 20, 20));
            text.HorizontalAlignment = HorizontalAlignment.Center;
            text.VerticalAlignment = VerticalAlignment.Center;

            Entity.Child = text;

            Entity.Margin = new Thickness(pos.X, pos.Y, 0, 0);
        }

        /// <summary>
        /// Moves node to position
        /// </summary>
        /// <param name="pos">Position to move</param>
        public void MoveTo(Point pos)
        {
            double x = pos.X - Entity.Width / 2;
            double y = pos.Y - Entity.Height / 2;
            Entity.Margin = new Thickness(x, y, 0, 0);
            Pos = new Point(x, y);
            
            if (NodeUpdate != null)
                NodeUpdate(this, null);
        }

        /// <summary>
        /// Moves node by a vector
        /// </summary>
        /// <param name="pos">A vector</param>
        public void MoveBy(Point pos)
        {
            Pos = new Point(Pos.X + pos.X, Pos.Y + pos.Y);
            Entity.Margin = new Thickness(Pos.X, Pos.Y, 0, 0);

            if (NodeUpdate != null)
                NodeUpdate(this, null);
        }

        /// <summary>
        /// Mouse enter handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse arguments</param>
        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            if (!_selected)
                Entity.Background = new SolidColorBrush(Color.FromRgb(225, 225, 225));
        }

        /// <summary>
        /// Mouse leave handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse arguments</param>
        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            if (!_selected)
                Entity.Background = new SolidColorBrush(Color.FromRgb(200, 200, 200));
        }

        /// <summary>
        /// Mouse down handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse button arguments</param>
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
                return;

            if (!Entity.IsMouseOver)
            {
                _selected = false;
                Entity.Background = new SolidColorBrush(Color.FromRgb(200, 200, 200));
            }
            else
            {
                if (ArcCommand.CanJoin())
                {
                    ArcCommand.Execute(this);
                }
                else
                {
                    _dragged = true;
                    UndoRedo.GetInstance().Enabled = false;
                    _selected = true;
                    Entity.Background = new SolidColorBrush(Color.FromRgb(150, 150, 150));
                    if (NodeUpdate != null) NodeUpdate(this, e);
                }
            }
        }

        /// <summary>
        /// Mouse move handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse arguments</param>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released && _moveCommand == null)
            {
                _dragged = false;
                UndoRedo.GetInstance().Enabled = true;
                return;
            }

            if (_dragged)
            {
                Grid grid = sender as Grid;
                if (grid == null) return;

                if (_moveCommand == null)
                {
                    Point nodePos = new Point
                    {
                        X = Entity.Margin.Left + Entity.Width/2,
                        Y = Entity.Margin.Top + Entity.Height/2
                    };
                    _moveCommand = new MoveNodeCommand(this, nodePos);
                }

                Point pos = e.GetPosition(grid);
                MoveTo(pos);

                if (e.LeftButton == MouseButtonState.Released)
                {
                    _dragged = false;
                    if (_moveCommand != null)
                    {
                        _moveCommand.Stop = e.GetPosition(grid);
                        UndoRedo.GetInstance().AddCommand(_moveCommand);
                        _moveCommand = null;
                    }
                }

                if (NodeUpdate != null) NodeUpdate(this, e);
            }
        }

        /// <summary>
        /// Property changed event
        /// </summary>
        /// <param name="property"></param>
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        /// <summary>
        /// Returns the center of the node
        /// </summary>
        /// <returns>The center of the node</returns>
        public Point GetNodeCenter()
        {
            Point pos = Pos;
            pos.X += Entity.Width / 2;
            pos.Y += Entity.Height / 2;
            return pos;
        }

        /// <summary>
        /// Adds handlers to a grid and the entity
        /// </summary>
        /// <param name="grid">A grid to add handlers to</param>
        public void AddHandlers(Grid grid)
        {
            grid.MouseDown += OnMouseDown;
            grid.MouseMove += OnMouseMove;
            Entity.MouseEnter += OnMouseEnter;
            Entity.MouseLeave += OnMouseLeave;
        }

        /// <summary>
        /// Remove handlers from a grid and the entity
        /// </summary>
        /// <param name="grid">A grid from which we remove handlers</param>
        public void RemoveHandlers(Grid grid)
        {
            grid.MouseDown -= OnMouseDown;
            grid.MouseMove -= OnMouseMove;
            Entity.MouseEnter -= OnMouseEnter;
            Entity.MouseLeave -= OnMouseLeave;
        }

        /// <summary>
        /// Adds node to a grid
        /// </summary>
        /// <param name="grid">A grid to add the node to</param>
        public void AddToGrid(Grid grid)
        {
            grid.Children.Add(Entity);
        }

        /// <summary>
        /// Removes node from a grid
        /// </summary>
        /// <param name="grid">A grid from which we remove the node</param>
        public void RemoveFromGrid(Grid grid)
        {
            grid.Children.Remove(Entity);
        }
    }
}
