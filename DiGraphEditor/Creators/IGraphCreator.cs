﻿namespace DiGraphEditor.Creators
{
    /// <summary>
    /// Interface for creating graph and graph elements
    /// </summary>
    public interface IGraphCreator
    {
        /// <summary>
        /// Creates new element
        /// </summary>
        /// <returns>Created element</returns>
        dynamic Create();
    }
}
