﻿using System;
using System.Collections;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Controls;
using DiGraphEditor.Utils;

namespace DiGraphEditor.Creators
{
    /// <summary>
    /// Creates graphs
    /// </summary>
    class GraphCreator : IGraphCreator
    {
        private readonly Grid _grid;
        private dynamic _graph;
        private readonly Delegate _nodeClickHandler;
        private readonly Delegate _arcClickHandler;

        /// <summary>
        /// Optional parameter which specifies the old graph from which we will create a new one.
        /// New graph will have new nodes and arcs like in the old graph and also values of
        /// the new graph will be the same as the old if it is possible (Types are the same for
        /// node and/or arc)
        /// </summary>
        public dynamic OldGraph { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="nodeHandler">On node update handler</param>
        /// <param name="arcHandler">On arc update handler</param>
        public GraphCreator(Grid aGrid, Delegate nodeHandler, Delegate arcHandler)
        {
            _grid = aGrid;
            _arcClickHandler = arcHandler;
            _nodeClickHandler = nodeHandler;
        }

        /// <summary>
        /// Creates new graph
        /// </summary>
        /// <returns>Created graph</returns>
        public dynamic Create()
        {
            Type graphType = Util.GetInstance().GetGenericGraphType();
            _graph = Activator.CreateInstance(graphType);

            if (OldGraph != null)
            {
                CreateNodes();
                CreateArcs();
            }

            return _graph;
        }

        /// <summary>
        /// Creates nodes from the old graph and adds them to the new one
        /// </summary>
        private void CreateNodes()
        {
            NodeCreator nodeCreator = new NodeCreator(_grid, _nodeClickHandler, _graph);

            foreach (dynamic node in OldGraph.Nodes)
                nodeCreator.Create(node.Pos, node.Value);
        }

        /// <summary>
        /// Creates arcs from the old graph and adds them to the new one
        /// </summary>
        private void CreateArcs()
        {
            foreach (dynamic arc in OldGraph.GetAllArcs())
            {
                ArcCreator arcCreator = new ArcCreator(_grid, _arcClickHandler);

                dynamic from;
                dynamic to;

                Type nodeType = Util.GetInstance().GetGenericBaseNodeType();

                MethodInfo method = _graph.GetType().GetMethod("NodesWhere");
                string sentence = "x => ex.Id s== cnodeId";
                int nodeId = arc.From.Id;
                LambdaExpression exp = LambdaCreator.SimpleCreate(nodeType, sentence, new object[] { nodeId });

                IEnumerator enumerator = ((IEnumerable)method.Invoke(_graph, new object[] { exp.Compile() })).GetEnumerator();
                if (enumerator.MoveNext())
                    from = enumerator.Current;
                else
                    continue;

                sentence = "x => ex.Id s== cnodeId";
                nodeId = arc.To.Id;
                exp = LambdaCreator.SimpleCreate(nodeType, sentence, new object[] { nodeId });

                enumerator = ((IEnumerable)method.Invoke(_graph, new object[] { exp.Compile() })).GetEnumerator();
                if (enumerator.MoveNext())
                    to = enumerator.Current;
                else
                    continue;

                arcCreator.Create(from, to, arc.Value);
            }
        }
    }
}
