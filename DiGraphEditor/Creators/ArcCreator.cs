﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Utils;

namespace DiGraphEditor.Creators
{
    /// <summary>
    /// Class for creating arcs
    /// </summary>
    public class ArcCreator
    {
        private dynamic _arc;
        private readonly Grid _grid;
        private readonly Delegate _entityClickHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="entityHandler">On arc update handler</param>
        public ArcCreator(Grid aGrid, Delegate entityHandler)
        {
            _grid = aGrid;
            _entityClickHandler = entityHandler;
        }

        /// <summary>
        /// Creates an arc
        /// </summary>
        /// <param name="from">Starting node of the arc (optional)</param>
        /// <param name="to">Ending node of the arc (optional)</param>
        /// <param name="val">Value of the arc (optional)</param>
        /// <returns>New arc</returns>
        public dynamic Create(dynamic from = null, dynamic to = null, dynamic val = null)
        {
            Type arcType = Util.GetInstance().GetGenericArcType();

            if (CheckIfCorrect(to))
            {
                _arc.RemoveFromGrid(_grid);
                _arc = null;
                return null;
            }

            if (_arc == null)
            {
                if (@from == null)
                    return null;

                StartArc(@from, to, arcType);
            }
            
            if (to != null)
            {
                EndArc(to, val);
            }

            return _arc;
        }

        /// <summary>
        /// Checks if arc is correct
        /// </summary>
        /// <param name="to">Ending node of the arc</param>
        /// <returns>True if arc is incorrect</returns>
        private bool CheckIfCorrect(dynamic to)
        {
            return _arc != null && (to == null || to.Equals(_arc.From) || _arc.From[to] != null);
        }

        /// <summary>
        /// Creates arc and maintains it state according to nodes
        /// </summary>
        /// <param name="from">Starting node of the arc</param>
        /// <param name="to">Ending node of the arc</param>
        /// <param name="arcType">Value of the arc</param>
        private void StartArc(dynamic from, dynamic to, Type arcType)
        {
            _arc = Activator.CreateInstance(arcType, from);

            if (to == null)
            {
                MouseEventHandler del = Delegate.CreateDelegate(typeof(MouseEventHandler), _arc, arcType.GetMethod("OnMouseMove"));
                _grid.MouseMove += del;
            }

            _arc.AddToGrid(_grid);
        }

        /// <summary>
        /// Joins the arc to the ending node
        /// </summary>
        /// <param name="to">Ending node of the arc</param>
        /// <param name="val">Value of the arc</param>
        private void EndArc(dynamic to, dynamic val)
        {
            _arc.SetEnd(to);
            AddHandlers(_arc);

            Line element = _arc.GetType().GetProperty("Entity").GetValue(_arc) as Line;
            CreateContextMenu(_arc, element);

            _arc.From[to] = _arc;

            _arc.ArcUpdate += (EntityChangedEventHandler)_entityClickHandler;

            PropertyInfo arcInfo = _arc.GetType().GetProperty("Value");

            if (val != null && val.GetType() == arcInfo.PropertyType)
                _arc.Value = val;
        }

        /// <summary>
        /// Adds handlers to the final arc (where from and to are specified)
        /// </summary>
        /// <param name="obj">The arc</param>
        /// <param name="element">Null</param>
        public void AddHandlers(dynamic obj, FrameworkElement element = null)
        {
            Type arcType = Util.GetInstance().GetGenericArcType();
            MouseEventHandler del = Delegate.CreateDelegate(typeof(MouseEventHandler), _arc, arcType.GetMethod("OnMouseMove"));
            _grid.MouseMove -= del;
            obj.AddHandlers(_grid);
        }

        /// <summary>
        /// Creates context menu for the arc
        /// </summary>
        /// <param name="obj">The arc</param>
        /// <param name="element">It's entity to which we will bind the context menu to</param>
        private void CreateContextMenu(object obj, Line element)
        {
            ContextMenu menu = new ContextMenu();
            MenuItem deleteItem = new MenuItem() { Header = "Delete", Command = AppManager.GetInstance().DeleteArc, CommandParameter = obj };

            menu.Items.Add(deleteItem);

            element.ContextMenu = menu;
        }
    }
}
