﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using DiGraphEditor.GraphVisualizer;

namespace DiGraphEditor.Creators
{
    /// <summary>
    /// Creates new node
    /// </summary>
    public class NodeCreator
    {
        private readonly Grid _grid;
        private readonly dynamic _graph;
        private readonly Delegate _entityClickHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="entityHandler">On node update handler</param>
        /// <param name="aGraph">Graph to which we will add nodes</param>
        public NodeCreator(Grid aGrid, Delegate entityHandler, dynamic aGraph = null)
        {
            _graph = aGraph ?? AppManager.GetInstance().Graph;

            _grid = aGrid;
            _entityClickHandler = entityHandler;
        }

        /// <summary>
        /// Create a node
        /// </summary>
        /// <param name="pos">Position of the node</param>
        /// <param name="val">Value of the node</param>
        /// <returns>New node</returns>
        public dynamic Create(Point pos = default(Point), dynamic val = null)
        {
            dynamic node = _graph.AddNode();

            if (pos != default(Point))
            {
                pos.X += node.Entity.Width / 2;
                pos.Y += node.Entity.Height / 2;
                node.MoveTo(pos);
            }

            PropertyInfo nodeInfo = node.GetType().GetProperty("Value");

            if (val != null && (val.GetType() == nodeInfo.PropertyType))
                node.Value = val;

            node.NodeUpdate += (EntityChangedEventHandler)_entityClickHandler;

            node.AddHandlers(_grid);

            CreateContextMenu(node, node.Entity);
            node.ArcCommand = AppManager.GetInstance().AddArc;

            node.AddToGrid(_grid);

            return node;
        }

        /// <summary>
        /// Creates context menu for the node
        /// </summary>
        /// <param name="node">Node for which we create context menu</param>
        /// <param name="nodeBorder">Entity of the node to bind the context menu to</param>
        private void CreateContextMenu(object node, Border nodeBorder)
        {
            ContextMenu menu = new ContextMenu();
            MenuItem deleteItem = new MenuItem() { Header = "Delete", Command = AppManager.GetInstance().DeleteNode, CommandParameter = node };
            MenuItem addArcItem = new MenuItem() { Header = "Add Arc", Command = AppManager.GetInstance().AddArc, CommandParameter = node };

            menu.Items.Add(deleteItem);
            menu.Items.Add(addArcItem);

            nodeBorder.ContextMenu = menu;
        }
    }
}
