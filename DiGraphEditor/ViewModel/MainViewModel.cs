﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Properties;
using DiGraphEditor.Utils;
using Exp = System.Linq.Expressions;

namespace DiGraphEditor.ViewModel
{
    /// <summary>
    /// ViewModel class for MainWindow
    /// </summary>
    class MainViewModel
    {
        /// <summary>
        /// Currently selected element
        /// </summary>
        public dynamic Selected { get; set; }

        /// <summary>
        /// Instance of app manager
        /// </summary>
        public AppManager Manager { get; set; }

        private readonly StackPanel _details;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">Grid on which we will draw</param>
        /// <param name="aDetail">Detail panel where information about nodes and arcs will be shown</param>
        public MainViewModel(Grid aGrid, StackPanel aDetail)
        {
            Manager = AppManager.GetInstance();
            _details = aDetail;

            Manager.InitializeCommands(aGrid, _details, 
                new EntityChangedEventHandler(OnNodeUpdate), 
                new EntityChangedEventHandler(OnArcUpdate));
        }

        /// <summary>
        /// Node update handler
        /// </summary>
        /// <param name="sender">An updated node</param>
        /// <param name="e">Mouse arguments</param>
        private void OnNodeUpdate(dynamic sender, MouseEventArgs e)
        {
            Type nodeType = sender.GetType();

            if (Selected == null || !Selected.Equals(sender) || _details.Children.Count < 1)
            {
                _details.Children.Clear();

                Selected = sender;

                Panel panel = ValueContentCreator.CreatePositionPanel("PositionGrid", "Position", sender);

                _details.Children.Add(panel);

                PropertyInfo nodeValue = nodeType.GetProperty("Value");
                int depth = (int)Settings.Default["MaxDepth"];

                _details.Children.Add(ValueContentCreator.CreateDeleteButton(sender));
                _details.Children.Add(new Separator());
                ValueContentCreator.GetNestedValues(_details, sender, nodeValue, depth);
            }
            else
            {
                Grid panel = _details.Children.OfType<Grid>().First(x => x.Name == "PositionGrid");
                TextBlock position = panel.Children.OfType<TextBlock>().First(x => x.Name == "PositionValue");
                position.Text = "<" + sender.Pos + ">";
            }

            dynamic graph = AppManager.GetInstance().Graph;
            MethodInfo method = graph.GetType().GetMethod("ArcsWhere");
            int nodeId = sender.Id;
            Type arcType = Util.GetInstance().GetGenericArcType();
            string sentence = "x => ex.To.Id s== cnodeId lor ex.From.Id s== cnodeId";
            Exp.LambdaExpression exp = LambdaCreator.SimpleCreate(Util.GetInstance().GetGenericBaseArcType(), sentence, new object[] { nodeId, nodeId });
            
            foreach (object arc in (IEnumerable)method.Invoke(graph, new object[] { exp.Compile() }))
            {
                arcType.GetMethod("UpdateArc").Invoke(arc, new object[0]);
            }
        }

        /// <summary>
        /// Arc update handler
        /// </summary>
        /// <param name="sender">An updated arc</param>
        /// <param name="e">Mouse arguments</param>
        private void OnArcUpdate(dynamic sender, MouseEventArgs e)
        {
            Type arcType = sender.GetType();

            if (e == null || arcType != Util.GetInstance().GetGenericArcType())
                return;

            if (Selected == null || !Selected.Equals(sender) || _details.Children.Count < 1)
            {
                _details.Children.Clear();

                Selected = sender;

                dynamic node = sender.From;
                Panel startPanel = ValueContentCreator.CreatePositionPanel("StartPositionGrid", "Start", node);
                _details.Children.Add(startPanel);

                node = sender.To;
                Panel stopPanel = ValueContentCreator.CreatePositionPanel("StopPositionGrid", "Stop", node);

                _details.Children.Add(stopPanel);

                PropertyInfo arcValue = arcType.GetProperty("Value");
                int depth = (int)Settings.Default["MaxDepth"];

                _details.Children.Add(ValueContentCreator.CreateDeleteButton(sender));
                _details.Children.Add(new Separator());
                ValueContentCreator.GetNestedValues(_details, sender, arcValue, depth);
            }
            else
            {
                dynamic nodeFrom = sender.From;
                dynamic nodeTo = sender.To;
                Grid panel = _details.Children.OfType<Grid>().First(x => x.Name == "StartPositionGrid");
                TextBlock position = panel.Children.OfType<TextBlock>().First(x => x.Name == "PositionValue");
                position.Text = "<" + nodeFrom.Pos + ">";
                panel = _details.Children.OfType<Grid>().First(x => x.Name == "StopPositionGrid");
                position = panel.Children.OfType<TextBlock>().First(x => x.Name == "PositionValue");
                position.Text = "<" + nodeTo.Pos + ">";
            }
        }

        /// <summary>
        /// Mouse down handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Mouse button arguments</param>
        public void OnMouseDown(dynamic sender, MouseButtonEventArgs e)
        {
            if (Manager.AddArc.CanJoin())
                Manager.AddArc.Execute(null);
        }
    }
}
