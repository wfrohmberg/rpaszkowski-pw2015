﻿using System.IO;
using System.Windows;
using DiGraphEditor.Reflection;
using Microsoft.Win32;

namespace DiGraphEditor.ViewModel
{
    /// <summary>
    /// ViewModel class for settings window
    /// </summary>
    class SettingsViewModel
    {
        /// <summary>
        /// Instance of assembly manager
        /// </summary>
        public AssemblyManager Manager { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aResource">A resource containing all available types</param>
        public SettingsViewModel(ResourceDictionary aResource)
        {
            Manager = AssemblyManager.GetInstance();
            Manager.Resource = aResource;
        }

        /// <summary>
        /// Assembly add button handler
        /// </summary>
        public void AddAssemblyClick()
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                DefaultExt = "/dll",
                Filter = "Dynamic-Link Libraries (*.dll)|*.dll|Executable (*.exe)|*.exe",
                InitialDirectory = Directory.GetCurrentDirectory()
            };

            bool? result = dlg.ShowDialog();

            if (result != null && result == true)
            {
                string filename = dlg.FileName;
                Manager.AddAssembly(filename);
            }
        }

        /// <summary>
        /// Assembly remove button handler
        /// </summary>
        /// <param name="name">Name of the assembly to remove</param>
        public void RemoveAssemblyClick(string name)
        {
            Manager.RemoveAssembly(name);
        }
    }
}
