﻿using System;
using System.Windows.Controls;
using DiGraphEditor.Commands;
using DiGraphEditor.Utils;

namespace DiGraphEditor
{
    /// <summary>
    /// Application manager
    /// </summary>
    public class AppManager
    {
        private static AppManager _instance;

        /// <summary>
        /// Current graph
        /// </summary>
        public dynamic Graph;

        #region Commands
        /// <summary>
        /// Save command
        /// </summary>
        public SaveCommand Save { get; set; }
        /// <summary>
        /// Load command
        /// </summary>
        public LoadCommand Load { get; set; }
        /// <summary>
        /// Add node command
        /// </summary>
        public AddNodeCommand AddNode { get; set; }
        /// <summary>
        /// Add arc command
        /// </summary>
        public AddArcCommand AddArc { get; set; }
        /// <summary>
        /// Exit command
        /// </summary>
        public ExitCommand Exit { get; set; }
        /// <summary>
        /// Create new graph command
        /// </summary>
        public NewCommand NewGraph { get; set; }
        /// <summary>
        /// Undo command
        /// </summary>
        public UndoCommand Undo { get; set; }
        /// <summary>
        /// Redo command
        /// </summary>
        public RedoCommand Redo { get; set; }
        /// <summary>
        /// Delete node command
        /// </summary>
        public DeleteNodeCommand DeleteNode { get; set; }
        /// <summary>
        /// Delete arc command
        /// </summary>
        public DeleteArcCommand DeleteArc { get; set; }
        /// <summary>
        /// Open window command
        /// </summary>
        public OpenWindowCommand OpenWin { get; set; }
        /// <summary>
        /// Change type command
        /// </summary>
        public ChangeCommand Change { get; set; }
        /// <summary>
        /// Apply layout command
        /// </summary>
        public LayoutCommand LayoutComm { get; set; }
        #endregion

        private AppManager() 
        {
            InitializeGraph();
        }

        /// <summary>
        /// Gets an _instance of the application manager
        /// </summary>
        /// <returns>An _instance of the application manager</returns>
        public static AppManager GetInstance()
        {
            return _instance ?? (_instance = new AppManager());
        }

        /// <summary>
        /// Initializes graph
        /// </summary>
        private void InitializeGraph()
        {
            Type graphType = Util.GetInstance().GetGenericGraphType();
            Graph = Activator.CreateInstance(graphType);
        }

        /// <summary>
        /// Initializes all commands
        /// </summary>
        /// <param name="grid">Drawing grid where graph elements will be drawn</param>
        /// <param name="details">Details panel where information about nodes and arcs will be shown</param>
        /// <param name="onNodeUpdate">Node update handler</param>
        /// <param name="onArcUpdate">Arc update handler</param>
        public void InitializeCommands(Grid grid, StackPanel details, Delegate onNodeUpdate, Delegate onArcUpdate)
        {
            //Create commands
            NewGraph = new NewCommand(grid, details);
            Save = new SaveCommand();
            Load = new LoadCommand(grid, onNodeUpdate, onArcUpdate);
            DeleteArc = new DeleteArcCommand(grid, details);
            DeleteNode = new DeleteNodeCommand(grid, details);
            AddArc = new AddArcCommand(grid, onArcUpdate);
            AddNode = new AddNodeCommand(grid, onNodeUpdate);
            Change = new ChangeCommand(grid, details, onNodeUpdate, onArcUpdate);
            LayoutComm = new LayoutCommand();
            OpenWin = new OpenWindowCommand();
            Undo = new UndoCommand();
            Redo = new RedoCommand();
            Exit = new ExitCommand();
        }
    }
}
