﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace DiGraphEditor.Layouts
{
    /// <summary>
    /// Tree layout
    /// </summary>
    public class TreeLayoutStrategy : IGraphLayoutStrategy
    {
        /// <summary>
        /// Applies tree layout, or circle layout if node count is too small, to a graph
        /// </summary>
        /// <param name="graph">A graph to which the layout will be applied</param>
        public void Apply(dynamic graph)
        {
            if (graph.Nodes.Count < 3)
            {
                CircleLayoutStrategy strategy = new CircleLayoutStrategy();
                strategy.Apply(graph);
            } 
            else
            {
                ApplyTree(graph);
            }
        }

        /// <summary>
        /// Applies tree layout to the graph
        /// </summary>
        /// <param name="graph">A graph to which the layout will be applied</param>
        private void ApplyTree(dynamic graph)
        {
            const int offset = 35;

            List<List<dynamic>> levels = CreateLevels(graph);
            List<dynamic> visited = new List<dynamic>();

            CirclePlacer placer = new CirclePlacer();
            int radius = 0;
            foreach (List<dynamic> level in levels)
            {
                if (level == levels.First())
                {
                    dynamic node = level.First();
                    Point pos = new Point();
                    level.First().MoveTo(pos);
                    visited.Add(node);

                    radius += offset;
                }
                else
                {
                    int previous = radius;
                    radius = placer.Place(level, radius);
                    radius += offset;
                    MoveVisited(visited, radius - previous);
                    visited.AddRange(level);
                }
            }
        }

        /// <summary>
        /// Creates levels for layout
        /// </summary>
        /// <param name="graph">A graph containing nodes</param>
        /// <returns>Levels of nodes for layout</returns>
        private List<List<dynamic>> CreateLevels(dynamic graph)
        {
            List<dynamic> nodes = new List<dynamic>(graph.Nodes);
            nodes.Sort((x, y) => -1 * x.Degree.CompareTo(y.Degree));

            List<List<dynamic>> levels = new List<List<dynamic>>();

            int last = -1;
            levels.Add(new List<dynamic>());

            foreach (dynamic node in nodes)
            {
                if (last == node.Degree || node == nodes.First())
                {
                    levels.Last().Add(node);
                }
                else
                {
                    last = node.Degree;
                    levels.Add(new List<dynamic>());
                    levels.Last().Add(node);
                }
            }
            return levels;
        }

        /// <summary>
        /// Moves already visited nodes after putting new nodes
        /// </summary>
        /// <param name="visited">A list of already visited nodes</param>
        /// <param name="distance">A distance to move nodes by</param>
        private void MoveVisited(List<dynamic> visited, int distance)
        {
            Point pos = new Point(distance, distance);
            foreach(dynamic node in visited)
            {
                node.MoveBy(pos);
            }
        }
    }
}
