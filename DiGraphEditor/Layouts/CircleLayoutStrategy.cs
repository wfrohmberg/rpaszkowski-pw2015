﻿using System.Collections.Generic;

namespace DiGraphEditor.Layouts
{
    /// <summary>
    /// Circle layout
    /// </summary>
    public class CircleLayoutStrategy : IGraphLayoutStrategy
    {
        /// <summary>
        /// Applies the circle layout to the graph
        /// </summary>
        /// <param name="graph">A graph to which the layout will be applied</param>
        public void Apply(dynamic graph)
        {
            CirclePlacer placer = new CirclePlacer();
            List<dynamic> nodes = new List<dynamic>(graph.Nodes);
            placer.Place(nodes);
        }
    }
}
