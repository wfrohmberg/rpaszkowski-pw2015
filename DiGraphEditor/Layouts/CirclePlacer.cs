﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace DiGraphEditor.Layouts
{
    /// <summary>
    /// For placing nodes in circle
    /// </summary>
    public class CirclePlacer
    {
        /// <summary>
        /// Places nodes in circle so they don't overlap and are far enough from each other
        /// </summary>
        /// <param name="nodes">Nodes to place</param>
        /// <param name="radius">Minimum radius</param>
        /// <returns>The center of the circle</returns>
        public int Place(List<dynamic> nodes, int radius = 0)
        {
            const int margin = 15;
            const int offset = 20;

            int count = nodes.Count;
            double angle = (Math.PI * 2) / count;
            int minDis = (int)(nodes[0].Entity.Width / 2 + offset);
            if (radius == 0)
                radius = minDis;

            bool correct = true;

            do {
                if (correct == false)
                    correct = true;

                int dis = (int)(angle * radius / 2);
                if (dis < minDis)
                {
                    radius *= 2;
                    correct = false;
                }
            } while (!correct);

            int centerDis = radius + margin;
            Point center = new Point(centerDis, centerDis);
            Point start = new Point(center.X - radius, center.Y);

            for (int i = 0; i < count; i++)
            {
                double rot = angle * i;
                Point pos = new Point
                {
                    X = (int) (Math.Cos(rot)*(start.X - center.X) - Math.Sin(rot)*(start.Y - center.Y) + center.X),
                    Y = (int) (Math.Sin(rot)*(start.X - center.X) + Math.Cos(rot)*(start.Y - center.Y) + center.Y)
                };

                nodes[i].MoveTo(pos);
            }

            return centerDis;
        }
    }
}
