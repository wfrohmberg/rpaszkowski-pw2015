﻿namespace DiGraphEditor.Layouts
{
    /// <summary>
    /// Interface for layouts
    /// </summary>
    public interface IGraphLayoutStrategy
    {
        /// <summary>
        /// Applies layout
        /// </summary>
        /// <param name="graph">A graph to which the layout will be applied</param>
        void Apply(dynamic graph);
    }
}
