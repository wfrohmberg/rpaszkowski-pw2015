﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace DiGraphEditor.Layouts
{
    /// <summary>
    /// Hierarchical layout
    /// </summary>
    public class HierarchicalLayoutStrategy : IGraphLayoutStrategy
    {
        /// <summary>
        /// Applies hierarchical layout to the graph
        /// </summary>
        /// <param name="graph">A graph to which the layout will be applied</param>
        public void Apply(dynamic graph)
        {
            const int width = 50;
            const int height = 50;
            const int margin = 15;

            int maxCount;
            List<List<dynamic>> levels = CreateLevels(graph, out maxCount);

            int maxWidth = width * maxCount;

            for (int i = 0; i < levels.Count; i++)
            {
                List<dynamic> level = levels[i];
                int y = height * i + margin;

                for (int j = 0; j < level.Count; j++)
                {
                    dynamic node = level[j];
                    int x = (int)(width * (j - level.Count / 2.0) + margin + maxWidth / 2.0);

                    Point pos = new Point(x, y);
                    node.MoveTo(pos);
                }
            }
        }

        /// <summary>
        /// Creates levels of nodes
        /// </summary>
        /// <param name="graph">Graph to create levels from</param>
        /// <param name="maxCount">Out parameter which gives maximum number of nodes that appeared on one level</param>
        /// <returns>List of levels</returns>
        private List<List<dynamic>> CreateLevels(dynamic graph, out int maxCount)
        {
            List<List<dynamic>> levels = new List<List<dynamic>>();
            Dictionary<int, int> toVisit = new Dictionary<int, int>();
            Dictionary<int, bool> visited = new Dictionary<int,bool>();

            FindBest(graph, toVisit, visited);

            while (toVisit.Count != 0)
            {
                VisitNode(graph, levels, toVisit, visited);
            }

            List<dynamic> temp = CreateLastLevel(graph, visited);
            if (temp.Count > 0)
                levels.Add(temp);

            maxCount = levels.Select(level => level.Count).Max();

            return levels;
        }

        /// <summary>
        /// Finds the node with the highest degree
        /// </summary>
        /// <param name="graph">A graph containing nodes</param>
        /// <param name="toVisit">A list of nodes to visit which will be initialized</param>
        /// <param name="visited">A list of visited nodes which will be initialized</param>
        private void FindBest(dynamic graph, Dictionary<int, int> toVisit, Dictionary<int, bool> visited)
        {
            dynamic best = null;
            foreach (dynamic node in graph.Nodes)
            {
                if (best == null || best.Degree < node.Degree)
                    best = node;

                visited.Add(node.Id, false);
            }

            if (best != null)
            {
                visited[best.Id] = true;
                toVisit.Add(best.Id, 0);
            }
        }

        /// <summary>
        /// Visits node
        /// </summary>
        /// <param name="graph">A graph containing this node</param>
        /// <param name="levels">A list of levels to which we will add nodes</param>
        /// <param name="toVisit">A list of nodes to visit</param>
        /// <param name="visited">A list of visited nodes</param>
        private void VisitNode(dynamic graph, List<List<dynamic>> levels, Dictionary<int, int> toVisit, Dictionary<int, bool> visited)
        {
            dynamic current = graph[toVisit.First().Key];
            int level = toVisit[current.Id];

            if (levels.Count <= level)
                levels.Add(new List<dynamic>());

            levels[level].Add(current);
            toVisit.Remove(current.Id);

            VisitIncident(toVisit, visited, current, level);
        }

        /// <summary>
        /// Visits all incident nodes
        /// </summary>
        /// <param name="toVisit">A lsit of nodes to visit</param>
        /// <param name="visited">A list of visited nodes</param>
        /// <param name="current">The current node</param>
        /// <param name="level">A level of the current node</param>
        private void VisitIncident(Dictionary<int, int> toVisit, Dictionary<int, bool> visited, dynamic current, int level)
        {
            foreach (dynamic arc in current.Arcs)
            {
                dynamic node = arc.To;
                if (visited[node.Id] == false)
                {
                    visited[node.Id] = true;
                    toVisit.Add(node.Id, level + 1);
                }
            }
        }

        /// <summary>
        /// Creates level from remaining nodes
        /// </summary>
        /// <param name="graph">Graph containing nodes</param>
        /// <param name="visited">A list of visited nodes</param>
        /// <returns>Last level</returns>
        private List<dynamic> CreateLastLevel(dynamic graph, Dictionary<int, bool> visited)
        {
            IEnumerable<int> toAdd = GetLeftNodes(visited);

            List<dynamic> level = new List<dynamic>();

            int[] enumerable = toAdd as int[] ?? toAdd.ToArray();
            if (enumerable.Any())
                level.AddRange(from dynamic node in enumerable select graph[node]);

            return level;
        }

        /// <summary>
        /// Get not yet visited nodes
        /// </summary>
        /// <param name="visited">A list of visited nodes</param>
        /// <returns>A list of not yet visited nodes</returns>
        private IEnumerable<int> GetLeftNodes(Dictionary<int, bool> visited)
        {
            return from pair in visited
                   where pair.Value == false
                   select pair.Key;
        }
    }
}
