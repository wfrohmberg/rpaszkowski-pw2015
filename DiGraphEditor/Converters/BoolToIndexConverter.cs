﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DiGraphEditor.Converters
{
    /// <summary>
    /// Converter from boolean value to index of combobox
    /// </summary>
    public class BoolToIndexConverter : IValueConverter
    {
        /// <summary>
        /// Converts a boolean value to an index
        /// </summary>
        /// <param name="value">Boolean value</param>
        /// <param name="targetType">Null</param>
        /// <param name="parameter">Null</param>
        /// <param name="culture">Null</param>
        /// <returns>Index of the combobox</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? 0 : 1; 
        }

        /// <summary>
        /// Converts an index to a boolean value
        /// </summary>
        /// <param name="value">Index of the combobox</param>
        /// <param name="targetType">Null</param>
        /// <param name="parameter">Null</param>
        /// <param name="culture">Null</param>
        /// <returns>The boolean value</returns>
        public object ConvertBack(dynamic value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((int)value == 0);
        }
    }
}
