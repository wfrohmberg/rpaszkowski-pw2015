﻿using System.Windows;

namespace DiGraphEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void DiGraphEditor_Exit(object sender, ExitEventArgs e)
        {
            DiGraphEditor.Properties.Settings.Default.Save();
        }
    }
}
