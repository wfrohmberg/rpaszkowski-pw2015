﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Layouts;
using DiGraphEditor.Utils;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to apply layout to the graph
    /// </summary>
    public class LayoutCommand : IReversibleCommand
    {
        private readonly Dictionary<LayoutStrategies, IGraphLayoutStrategy> _strategies;
        private NodesMemento _mementoBefore;
        private NodesMemento _mementoAfter;

        /// <summary>
        /// Constructor
        /// </summary>
        public LayoutCommand()
        {
            _strategies = new Dictionary<LayoutStrategies, IGraphLayoutStrategy>
            {
                {LayoutStrategies.Circle, new CircleLayoutStrategy()},
                {LayoutStrategies.Tree, new TreeLayoutStrategy()},
                {LayoutStrategies.Hierarchical, new HierarchicalLayoutStrategy()}
            };
        }

        /// <summary>
        /// Executes the layout command
        /// </summary>
        /// <param name="parameter">Type of layout to apply</param>
        public void Execute(object parameter)
        {
            dynamic graph = AppManager.GetInstance().Graph;

            if (_mementoAfter == null)
            {
                _mementoBefore = CreateMemento(graph);
                _strategies[(LayoutStrategies)parameter].Apply(graph);
                _mementoAfter = CreateMemento(graph);

                UndoRedo.GetInstance().AddCommand(Clone());

                _mementoBefore = null;
                _mementoAfter = null;
            }
            else
            {
                SetMemento(graph, _mementoAfter);
            }
        }

        /// <summary>
        /// Unexecutes the layout command
        /// </summary>
        public void UnExecute()
        {
            dynamic graph = AppManager.GetInstance().Graph;
            SetMemento(graph, _mementoBefore);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return (AppManager.GetInstance().Graph.Nodes.Count >= 1);
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Creates a memento of nodes in graph
        /// </summary>
        /// <param name="graph">Current graph</param>
        /// <returns>Memento of nodes</returns>
        private NodesMemento CreateMemento(dynamic graph)
        {
            NodesMemento memento = new NodesMemento();
            foreach(dynamic node in graph.Nodes)
            {
                memento.AddToState(node.Id, node.Pos);
            }
            return memento;
        }

        /// <summary>
        /// Sets the memento of nodes
        /// </summary>
        /// <param name="graph">A graph on which we will apply memento</param>
        /// <param name="memento">Memento from which we will get nodes states</param>
        private void SetMemento(dynamic graph, NodesMemento memento)
        {
            Dictionary<int, Point> state = memento.GetState();
            foreach(dynamic node in graph.Nodes)
            {
                Point pos = state[node.Id];
                pos.X += node.Entity.Width / 2;
                pos.Y += node.Entity.Height / 2;

                node.MoveTo(pos);
            }
        }

        /// <summary>
        /// Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            LayoutCommand command = new LayoutCommand
            {
                _mementoBefore = _mementoBefore,
                _mementoAfter = _mementoAfter
            };

            return command;
        }
    }
}
