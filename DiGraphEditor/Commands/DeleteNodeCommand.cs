﻿using System;
using System.Collections;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Utils;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to delete node from the current graph
    /// </summary>
    public class DeleteNodeCommand : IReversibleCommand
    {
        private dynamic _node;
        private readonly Grid _grid;
        private readonly StackPanel _nodeDetails;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="aDetail">Detail panel where node and arc info is located</param>
        public DeleteNodeCommand(Grid aGrid, StackPanel aDetail)
        {
            _grid = aGrid;
            _nodeDetails = aDetail;
        }

        /// <summary>
        /// Executes the delete node command
        /// </summary>
        /// <param name="parameter">Node to add</param>
        public void Execute(object parameter)
        {
            dynamic graph = AppManager.GetInstance().Graph;

            if (_node == null)
            {
                _node = parameter;

                MethodInfo method = graph.GetType().GetMethod("ArcsWhere");
                int nodeId = (int)_node.GetType().GetProperty("Id").GetValue(_node);
                string sentence = "x => ex.To.Id s== cnodeId lor ex.From.Id s== cnodeId";
                LambdaExpression exp = LambdaCreator.SimpleCreate(Util.GetInstance().GetGenericBaseArcType(), sentence, new object[] { nodeId, nodeId });

                foreach (object arc in (IEnumerable)method.Invoke(graph, new object[] { exp.Compile() }))
                    AppManager.GetInstance().DeleteArc.Execute(arc);

                graph.RemoveNode(_node);
                _node.RemoveFromGrid(_grid);
                _node.RemoveHandlers(_grid);
                _nodeDetails.Children.Clear();

                UndoRedo.GetInstance().AddCommand(Clone());

                _node = null;
            }
            else
            {
                graph.RemoveNode(_node);
                _node.RemoveFromGrid(_grid);
                _node.RemoveHandlers(_grid);
                _nodeDetails.Children.Clear();
            }
        }

        /// <summary>
        /// Unexecutes the delete node command
        /// </summary>
        public void UnExecute()
        {
            AppManager.GetInstance().Graph.InsertNode(_node);
            _node.AddToGrid(_grid);
            _node.AddHandlers(_grid);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            if (parameter != null && UndoRedo.GetInstance().Enabled) return true;
            else return false;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            DeleteNodeCommand command = new DeleteNodeCommand(_grid, _nodeDetails) {_node = _node};
            return command;
        }
    }
}
