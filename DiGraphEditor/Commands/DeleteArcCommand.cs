﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to delete arc from the current graph
    /// </summary>
    public class DeleteArcCommand : IReversibleCommand
    {
        private dynamic _node;
        private dynamic _arc;
        private readonly Grid _grid;
        private bool _clone;
        private readonly StackPanel _details;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="aDetail">Detail panel where node and arc info is located</param>
        public DeleteArcCommand(Grid aGrid, StackPanel aDetail)
        {
            _grid = aGrid;
            _clone = false;
            _details = aDetail;
        }

        /// <summary>
        /// Executes the delete arc command
        /// </summary>
        /// <param name="parameter">Arc to delete</param>
        public void Execute(dynamic parameter)
        {
            if (_arc == null)
                _arc = parameter;

            _node = _arc.From;
            dynamic toNode = _arc.To;
            _node[toNode] = null;
            _arc.RemoveFromGrid(_grid);
            _arc.RemoveHandlers(_grid);

            _details.Children.Clear();

            if (!_clone)
            {
                UndoRedo.GetInstance().AddCommand(Clone());
                _arc = null;
            }
        }

        /// <summary>
        /// UnExecutes the delete arc command
        /// </summary>
        public void UnExecute()
        {
            dynamic toNode = _arc.To;
            _node[toNode] = _arc;
            _arc.AddToGrid(_grid);
            _arc.AddHandlers(_grid);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().Enabled;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            DeleteArcCommand command = new DeleteArcCommand(_grid, _details)
            {
                _arc = _arc,
                _node = _node,
                _clone = true
            };
            return command;
        }
    }
}
