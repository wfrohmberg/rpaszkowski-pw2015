﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Creators;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to add _node to the current graph
    /// </summary>
    public class AddNodeCommand : IReversibleCommand
    {
        private dynamic _node;
        private readonly Grid _grid;
        private readonly Delegate _entityClickHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A _grid to which we add the _node</param>
        /// <param name="entityHandler">Handler to delegate _node update events to</param>
        public AddNodeCommand(Grid aGrid, Delegate entityHandler)
        {
            _grid = aGrid;
            _entityClickHandler = entityHandler;
        }

        /// <summary>
        /// Executes the add _node command
        /// </summary>
        /// <param name="parameter">Null or position where to add the _node</param>
        public void Execute(object parameter)
        {
            NodeCreator creator = new NodeCreator(_grid, _entityClickHandler);
            IInputElement element = parameter as IInputElement;
            Point pos = new Point();
            if (element != null)
                pos = Mouse.GetPosition(element);

            if (_node == null)
            {
                _node = creator.Create(pos);
                UndoRedo.GetInstance().AddCommand(Clone());
                _node = null;
            }
            else
            {
                AppManager.GetInstance().Graph.AddNode(_node);
                _node.AddHandlers(_grid);

                _node.AddToGrid(_grid);
            }
        }

        /// <summary>
        /// Unexecutes the add _node command
        /// </summary>
        public void UnExecute()
        {
            AppManager.GetInstance().Graph.RemoveNode(_node);

            _node.RemoveFromGrid(_grid);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().Enabled;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            AddNodeCommand clone = new AddNodeCommand(_grid, _entityClickHandler) {_node = _node};
            return clone;
        }
    }
}
