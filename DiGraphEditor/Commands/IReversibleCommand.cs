﻿using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Interface for commands that are reversible
    /// </summary>
    public interface IReversibleCommand : ICommand
    {
        /// <summary>
        /// Method which will be run when we want to unexecute the command
        /// </summary>
        void UnExecute();

        /// <summary>
        /// Clones the command. Very useful, because in WPF you call commands from XAML so you have to place 
        /// the command in the UndoRedo list during execution of it
        /// </summary>
        /// <returns>Clone of this command</returns>
        IReversibleCommand Clone();
    }
}
