﻿using System;
using System.Windows;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to move node
    /// </summary>
    public class MoveNodeCommand : IReversibleCommand
    {
        private readonly Point _start;
        private Point _stop;
        private readonly dynamic _node;

        /// <summary>
        /// Point where to move the node
        /// </summary>
        public Point Stop { set { _stop = value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aNode">Node to move</param>
        /// <param name="aStart">Start point</param>
        public MoveNodeCommand(dynamic aNode, Point aStart)
        {
            _start = aStart;
            _node = aNode;
        }

        /// <summary>
        /// Executes the move node command
        /// </summary>
        /// <param name="parameter">Null</param>
        public void Execute(object parameter)
        {
            _node.MoveTo(_stop);
        }

        /// <summary>
        /// Unexecutes the move node command
        /// </summary>
        public void UnExecute()
        {
            _node.MoveTo(_start);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().Enabled;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            MoveNodeCommand command = new MoveNodeCommand(_node, _start) {_stop = _stop};
            return command;
        }
    }
}
