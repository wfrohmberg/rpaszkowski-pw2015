﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Properties;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to create new graph
    /// </summary>
    public class NewCommand : ICommand
    {
        private readonly Grid _grid;
        private readonly StackPanel _details;

        /// <summary>
        /// Specifies if creating new graph ended succesfully
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="aDetail">Detail panel where node and arc info is located</param>
        public NewCommand(Grid aGrid, StackPanel aDetail) 
        {
            _grid = aGrid;
            _details = aDetail;
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Executes the new command
        /// </summary>
        /// <param name="parameter">Warning message</param>
        public void Execute(object parameter)
        {
            string s = parameter as string;
            if (s != null)
            {
                string[] message = s.Split(';');
                Success = false;
                MessageBoxResult result = MessageBox.Show(message[1], message[0], MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    Success = true;
                    Settings.Default["SaveFile"] = "0";

                    dynamic graph = AppManager.GetInstance().Graph;
                    IVisitor rem = new RemoveVisitor(_grid);
                    rem.VisitGraph(graph);
                    graph.Clear();
                    _details.Children.Clear();
                    UndoRedo.GetInstance().ClearHistory();
                }
            }
        }
    }
}
