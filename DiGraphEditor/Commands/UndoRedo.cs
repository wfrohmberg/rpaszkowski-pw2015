﻿using System.Collections.Generic;
using DiGraphEditor.Properties;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Singleton class managing the UndoRedo feature
    /// </summary>
    public class UndoRedo
    {
        private static UndoRedo _instance;

        private int _historySize;
        private readonly LinkedList<IReversibleCommand> _history;
        private LinkedListNode<IReversibleCommand> _current;

        /// <summary>
        /// Specifies if UndoRedo feature is available
        /// </summary>
        public bool Enabled { get; set; }

        private UndoRedo() 
        {
            _history = new LinkedList<IReversibleCommand>();
            _historySize = (int) Settings.Default["HistorySize"];
            Enabled = true;
        }

        /// <summary>
        /// Sets the history size
        /// </summary>
        /// <param name="value">Size of the history</param>
        public void SetHistorySize(int value)
        {
            _historySize = value;
            _history.Clear();
            _current = null;
        }

        /// <summary>
        /// Clears current history
        /// </summary>
        public void ClearHistory()
        {
            _history.Clear();
            _current = null;
        }

        /// <summary>
        /// Returns instance of this class
        /// </summary>
        /// <returns>Instance of this class</returns>
        public static UndoRedo GetInstance()
        {
            return _instance ?? (_instance = new UndoRedo());
        }

        /// <summary>
        /// Checks if we can undo command
        /// </summary>
        /// <returns>True if there is a command to undo</returns>
        public bool CanUndo()
        {
            return (_history.Count > 0 && _current != null && Enabled);
        }

        /// <summary>
        /// Undo a command
        /// </summary>
        public void Undo()
        {
            if (_history.Count <= 0 || _current == null)
                return;

            IReversibleCommand command = _current.Value;
            _current = _current.Previous;
            command.UnExecute();
        }

        /// <summary>
        /// Checks if we can redo command
        /// </summary>
        /// <returns>True if there is a command to redo</returns>
        public bool CanRedo()
        {
            return (_history.Count > 0 && _current != _history.Last && Enabled);
        }

        /// <summary>
        /// Redo a command
        /// </summary>
        public void Redo()
        {
            if (_history.Count == 0 || _current == _history.Last)
                return;

            _current = _current == null ? _history.First : _current.Next;

            if (_current != null)
            {
                IReversibleCommand command = _current.Value;
                command.Execute(true);
            }
        }

        /// <summary>
        /// Adds a command to UndoRedo list
        /// </summary>
        /// <param name="command">Command to add</param>
        public void AddCommand(IReversibleCommand command)
        {
            if (_current == null)
            {
                if (_history.Count != 0)
                    _history.Clear();

                _history.AddFirst(command);
                _current = _history.First;
            }
            else
            {
                while (_history.Last != _current) _history.RemoveLast();

                if (_history.Count >= _historySize)
                    _history.RemoveFirst();

                _history.AddAfter(_current, command);
                _current = _history.Last;
            }
        }
    }
}
