﻿using System;
using System.IO;
using System.Windows.Input;
using DiGraphEditor.Packer;
using DiGraphEditor.Properties;
using Microsoft.Win32;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to save the graph to specified file
    /// </summary>
    public class SaveCommand : ICommand
    {
        /// <summary>
        /// Executes the save command
        /// </summary>
        /// <param name="parameter">Pass true if you want to use last save path as the current save path</param>
        public void Execute(object parameter)
        {
            string path = "0";
            bool? saveOption = parameter as bool?;
            if (saveOption != null && saveOption == true)
                path = (string)Settings.Default["SaveFile"];

            if (path == "0")
            {
                path = SaveDialog();
                if (String.IsNullOrEmpty(path))
                    return;
            }

            Settings.Default["SaveFile"] = path;
            WriteToFile(path, AppManager.GetInstance().Graph);
        }

        /// <summary>
        /// Shows save dialog
        /// </summary>
        /// <returns>Path to the file where the graph will be saved</returns>
        private string SaveDialog()
        {
            SaveFileDialog dlg = new SaveFileDialog
            {
                DefaultExt = ".dig",
                Filter = "DiGraph Files (*.dig)|*.dig",
                InitialDirectory = Directory.GetCurrentDirectory()
            };

            bool? result = dlg.ShowDialog();

            if (result == true)
                return dlg.FileName;

            return "";
        }

        /// <summary>
        /// Writes graph to file
        /// </summary>
        /// <param name="path">Path to the file</param>
        /// <param name="obj">Object to pack</param>
        private void WriteToFile(string path, dynamic obj)
        {
            XmlPacker packer = new XmlPacker();
            string xml = packer.Serialize(obj);

            packer.Pack(path, xml);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
