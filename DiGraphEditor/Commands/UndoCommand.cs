﻿using System;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to undo other commands
    /// </summary>
    public class UndoCommand : ICommand
    {
        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().CanUndo();
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Executes the undo command
        /// </summary>
        /// <param name="parameter">Null</param>
        public void Execute(object parameter)
        {
            UndoRedo.GetInstance().Undo();
        }
    }
}
