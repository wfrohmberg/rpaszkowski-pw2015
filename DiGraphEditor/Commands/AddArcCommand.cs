﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Creators;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to add _arc to the current graph
    /// </summary>
    public class AddArcCommand : IReversibleCommand
    {
        private dynamic _arc;
        private ArcCreator _creator;
        private readonly Grid _grid;
        private readonly Delegate _entityClickHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A _grid to which we add the _arc</param>
        /// <param name="entityHandler">Handler to delegate _arc update events to</param>
        public AddArcCommand(Grid aGrid, Delegate entityHandler)
        {
            _grid = aGrid;
            _entityClickHandler = entityHandler;
        }

        /// <summary>
        /// Executes the add _arc command
        /// </summary>
        /// <param name="parameter">Node to which connect the _arc (To connect _arc to two nodes you have to run this command twice)</param>
        public void Execute(dynamic parameter)
        {
            if (parameter != null && (!(parameter.GetType().Name.Contains("Node") || parameter is bool)))
                return;

            if (_arc == null)
            {
                UndoRedo.GetInstance().Enabled = false;
                _creator = new ArcCreator(_grid, _entityClickHandler);
                _arc = _creator.Create(from: parameter);
            }
            else
            {
                dynamic node = _arc.To;
                if (node == null)
                {
                    _arc = _creator.Create(to: parameter);

                    UndoRedo.GetInstance().Enabled = true;
                    _creator = null;

                    if (_arc != null)
                    {
                        UndoRedo.GetInstance().AddCommand(Clone());
                        _arc = null;
                    }
                }
                else
                {
                    dynamic fromNode = _arc.From;
                    dynamic toNode = _arc.To;
                    fromNode[toNode] = _arc;
                    _arc.AddToGrid(_grid);
                    _arc.AddHandlers(_grid);
                }
            }
        }

        /// <summary>
        /// Unexecutes the add _arc command
        /// </summary>
        public void UnExecute()
        {
            dynamic fromNode = _arc.From;
            dynamic toNode = _arc.To;
            fromNode[toNode] = null;
            _arc.RemoveFromGrid(_grid);
            _arc.RemoveHandlers(_grid);
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().Enabled;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            AddArcCommand clone = new AddArcCommand(_grid, _entityClickHandler) {_arc = _arc};
            return clone;
        }

        /// <summary>
        /// Specifies if _arc can be joined with any node
        /// </summary>
        /// <returns>True if _arc can be joined with any node</returns>
        public bool CanJoin()
        {
            if (_arc != null && _arc.From != null)
                return true;
            else
                return false;
        }
    }
}
