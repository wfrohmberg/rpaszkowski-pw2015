﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Creators;
using DiGraphEditor.Packer;
using DiGraphEditor.Properties;
using DiGraphEditor.Utils;
using Microsoft.Win32;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to load the graph
    /// </summary>
    public class LoadCommand : ICommand
    {
        private readonly Grid _grid;
        private readonly Delegate _nodeClickHandler;
        private readonly Delegate _arcClickHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="nodeHandler">On node update handler</param>
        /// <param name="arcHandler">On arc update handler</param>
        public LoadCommand(Grid aGrid, Delegate nodeHandler, Delegate arcHandler)
        {
            _grid = aGrid;
            _nodeClickHandler = nodeHandler;
            _arcClickHandler = arcHandler;
        }

        /// <summary>
        /// Executes the load command
        /// </summary>
        /// <param name="parameter">Warning message</param>
        public void Execute(object parameter)
        {
            string path = LoadDialog();
            if (!String.IsNullOrEmpty(path))
            {
                string msg = parameter as string;
                LoadFile(path, msg);
            }
        }

        /// <summary>
        /// Shows the loading dialog
        /// </summary>
        /// <returns>Path to the file from which we will get the graph</returns>
        private string LoadDialog()
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                DefaultExt = ".dig",
                Filter = "DiGraph Files (*.dig)|*.dig",
                InitialDirectory = Directory.GetCurrentDirectory()
            };

            bool? result = dlg.ShowDialog();

            return result == true ? dlg.FileName : "";
        }

        /// <summary>
        /// Loads the file from given path
        /// </summary>
        /// <param name="path">Path to the file</param>
        /// <param name="msg">Warning message</param>
        private void LoadFile(string path, string msg)
        {
            XmlPacker packer = new XmlPacker();
            string xml = packer.Unpack(path);

            if (AppManager.GetInstance().Graph.Nodes.Count > 0)
                AppManager.GetInstance().NewGraph.Execute(msg);
            else
                AppManager.GetInstance().NewGraph.Success = true;

            if (AppManager.GetInstance().NewGraph.Success)
            {
                Settings.Default["SaveFile"] = path;

                Type type = Util.GetInstance().GetGenericGraphType();
                MethodInfo method = packer.GetType().GetMethod("Deserialize");
                MethodInfo generic = method.MakeGenericMethod(type);
                dynamic graph = generic.Invoke(packer, new object[] { xml });

                InsertGraph(graph);
            }
        }

        /// <summary>
        /// Creates new graph using the given one and change it to the current graph
        /// </summary>
        /// <param name="graph">Graph template from which we create new graph</param>
        private void InsertGraph(dynamic graph)
        {
            GraphCreator creator = new GraphCreator(_grid, _nodeClickHandler, _arcClickHandler) { OldGraph = graph };
            AppManager.GetInstance().Graph = creator.Create();
        }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
