﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Creators;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Properties;
using DiGraphEditor.Utils;

namespace DiGraphEditor.Commands
{
    /// <summary>
    ///     Command to change the type of _graph
    /// </summary>
    public class ChangeCommand : IReversibleCommand
    {
        private readonly Delegate _arcClickHandler;
        private readonly StackPanel _details;
        private readonly Grid _grid;
        private readonly Delegate _nodeClickHandler;
        private bool _clone;
        private dynamic _graph;
        private dynamic _newGraph;
        private string _newType;
        private string _oldType;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="aGrid">A grid where nodes and arcs are placed</param>
        /// <param name="aDetail">Detail panel where node and arc info is located</param>
        /// <param name="nodeHandler">On node update handler</param>
        /// <param name="arcHandler">On arc update handler</param>
        public ChangeCommand(Grid aGrid, StackPanel aDetail, Delegate nodeHandler, Delegate arcHandler)
        {
            _grid = aGrid;
            _details = aDetail;
            _nodeClickHandler = nodeHandler;
            _arcClickHandler = arcHandler;
            _clone = false;
        }

        /// <summary>
        ///     Specifies if we change the nodes or arcs
        /// </summary>
        public ChangeOptions Option { get; set; }

        /// <summary>
        ///     Executes the change command
        /// </summary>
        /// <param name="parameter">New type</param>
        public void Execute(object parameter)
        {
            _graph = AppManager.GetInstance().Graph;
            IVisitor rem = new RemoveVisitor(_grid);
            rem.VisitGraph(_graph);
            _details.Children.Clear();

            string option = Option == ChangeOptions.Node ? "NodeType" : "ArcType";

            if (!_clone)
            {
                _oldType = Settings.Default[option] as string;
                _newType = parameter as string;
                Settings.Default[option] = _newType;
                var creator = new GraphCreator(_grid, _nodeClickHandler, _arcClickHandler) {OldGraph = _graph};

                _newGraph = creator.Create();
                AppManager.GetInstance().Graph = _newGraph;

                UndoRedo.GetInstance().AddCommand(Clone());
            }
            else
            {
                Settings.Default[option] = _newType;
                IVisitor add = new AddVisitor(_grid);
                add.VisitGraph(_newGraph);
            }
        }

        /// <summary>
        ///     Unexecutes the change command
        /// </summary>
        public void UnExecute()
        {
            string option = Option == ChangeOptions.Node ? "NodeType" : "ArcType";
            Settings.Default[option] = _oldType;

            IVisitor rem = new RemoveVisitor(_grid);
            rem.VisitGraph(_newGraph);

            IVisitor add = new AddVisitor(_grid);
            add.VisitGraph(_graph);
        }

        /// <summary>
        ///     Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().Enabled;
        }

        /// <summary>
        ///     Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        ///     Clones the command
        /// </summary>
        /// <returns>Reversible command</returns>
        public IReversibleCommand Clone()
        {
            ChangeCommand command = new ChangeCommand(_grid, _details, _nodeClickHandler, _arcClickHandler)
            {
                _clone = true,
                _oldType = _oldType,
                _newType = _newType,
                _newGraph = _newGraph,
                _graph = _graph,
                Option = Option
            };

            return command;
        }
    }
}