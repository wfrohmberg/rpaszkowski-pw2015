﻿using System;
using System.Windows;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to exit application
    /// </summary>
    public class ExitCommand : ICommand
    {
        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Executes the exit command
        /// </summary>
        /// <param name="parameter">null</param>
        public void Execute(object parameter)
        {
            Application.Current.Shutdown();
        }
    }
}
