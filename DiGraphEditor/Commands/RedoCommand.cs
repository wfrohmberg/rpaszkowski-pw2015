﻿using System;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to redo other commands
    /// </summary>
    public class RedoCommand : ICommand
    {
        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return UndoRedo.GetInstance().CanRedo();
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Executes the redo command
        /// </summary>
        /// <param name="parameter">Null</param>
        public void Execute(object parameter)
        {
            UndoRedo.GetInstance().Redo();
        }
    }
}
