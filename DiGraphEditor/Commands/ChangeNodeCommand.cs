﻿using DiGraphEditor.Creators;
using DiGraphEditor.GraphVisualizer;
using DiGraphEditor.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    public class ChangeNodeCommand : IReversibleCommand
    {
        private object graph;
        private object newGraph;
        private Canvas canvas;
        private Delegate nodeClickHandler;
        private Delegate arcClickHandler;

        public event TypeChangedEventHandler NodeTypeChanged;

        public DeleteNodeCommand DelNodeCommand { get; set; }
        public DeleteArcCommand DelArcCommand { get; set; }
        public AddArcCommand ArcCommand { get; set; }

        public ChangeNodeCommand(object aGraph, Canvas aCanvas, Delegate nodeHandler, Delegate arcHandler)
        {
            graph = aGraph;
            canvas = aCanvas;
            nodeClickHandler = nodeHandler;
            arcClickHandler = arcHandler;
        }

        public void Execute(object parameter)
        {
            Properties.Settings.Default["NodeType"] = parameter.GetType().ToString();
            Type graphType = Utils.Util.GetGenericGraphType();
            newGraph = Activator.CreateInstance(graphType);

            canvas.Children.Clear();

            NodeCreator nodeCreator = new NodeCreator(newGraph, canvas, nodeClickHandler)
                { DeleteCommand = DelNodeCommand, ArcCommand = ArcCommand };

            ArcCreator arcCreator = new ArcCreator(newGraph, canvas, arcClickHandler) 
                { DeleteCommand = DelArcCommand };

            foreach (object node in (IEnumerable)graph.GetType().GetProperty("Nodes").GetValue(graph))
            {
                nodeCreator.Pos = (Point) node.GetType().GetProperty("Pos").GetValue(node);
                nodeCreator.Create();
            }

            createArcs(arcCreator);
        }

        public void UnExecute()
        {
            throw new NotImplementedException();
        }

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public IReversibleCommand Clone()
        {
            throw new NotImplementedException();
        }

        private void createArcs(ArcCreator arcCreator)
        {
            foreach (object node in (IEnumerable)graph.GetType().GetProperty("Nodes").GetValue(graph))
            {
                foreach (object arc in (IEnumerable)node.GetType().GetProperty("Arcs").GetValue(node))
                {
                    object from = null;
                    object to = null;

                    MethodInfo method = newGraph.GetType().GetMethod("ArcsWhere");
                    string sentence = "x => ex.From.Id s== cnodeId";
                    object temp = arc.GetType().GetProperty("From").GetValue(arc);
                    int nodeId = (int) temp.GetType().GetProperty("Id").GetValue(temp);
                    LambdaExpression exp = LambdaCreator.SimpleCreate(Util.GetGenericBaseArcType(), sentence, new object[] { nodeId });

                    IEnumerator enumerable = ((IEnumerable)method.Invoke(newGraph, new object[] { exp.Compile() })).GetEnumerator();
                    if (enumerable.MoveNext())
                        from = enumerable.Current;
                    else
                        continue;

                    sentence = "x => ex.To.Id s== cnodeId";
                    temp = arc.GetType().GetProperty("To").GetValue(arc);
                    nodeId = (int)temp.GetType().GetProperty("Id").GetValue(temp);
                    exp = LambdaCreator.SimpleCreate(Util.GetGenericBaseArcType(), sentence, new object[] { nodeId });

                    enumerable = ((IEnumerable)method.Invoke(newGraph, new object[] { exp.Compile() })).GetEnumerator();
                    if (enumerable.MoveNext())
                        to = enumerable.Current;
                    else
                        continue;

                    arcCreator.From = from;
                    arcCreator.To = to;
                    arcCreator.Create();
                }
            }
        }
    }
}
