﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DiGraphEditor.Commands
{
    /// <summary>
    /// Command to open new window
    /// </summary>
    public class OpenWindowCommand : ICommand
    {
        /// <summary>
        /// A grid where nodes and arcs are placed
        /// </summary>
        public Grid WinGrid { get; set; }

        /// <summary>
        /// On node update handler
        /// </summary>
        public Delegate NodeHandler { get; set; }

        /// <summary>
        /// On arc update handler
        /// </summary>
        public Delegate ArcHandler { get; set; }

        /// <summary>
        /// Specifies if command can be executed
        /// </summary>
        /// <param name="parameter">Parameter passed to command</param>
        /// <returns>True if command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Notifies that the possibility of the execution has changed
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Executes the open window command
        /// </summary>
        /// <param name="parameter">Window to open</param>
        public void Execute(object parameter)
        {
            Window win = parameter as Window;
            SettingsWindow setWin = win as SettingsWindow;

            if (setWin != null)
                setWin.InitializeWindow(WinGrid, NodeHandler, ArcHandler);

            if (win != null)
            {
                win.Show();
                win.Focus();
            }
        }
    }
}
