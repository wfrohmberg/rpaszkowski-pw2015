﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using DiGraphAttribute;

namespace DiGraphEditor.Reflection
{
    /// <summary>
    /// Manages assemblies
    /// </summary>
    public class AssemblyManager
    {
        /// <summary>
        /// Collection of assembly names
        /// </summary>
        public ObservableCollection<string> AssembliesNames { get; set; }
        private readonly Dictionary<string, Assembly> _assemblies;

        private static AssemblyManager _instance;
        private ResourceDictionary _resource;

        /// <summary>
        /// Resource from which we get all types
        /// </summary>
        public ResourceDictionary Resource { get { return _resource; } set { _resource = value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        private AssemblyManager() 
        {
            _assemblies = new Dictionary<string, Assembly>();
            AssembliesNames = new ObservableCollection<string>();
        }

        /// <summary>
        /// Gets an instance of the assembly manager
        /// </summary>
        /// <returns>An instance of the assembly manager</returns>
        public static AssemblyManager GetInstance()
        {
            return _instance ?? (_instance = new AssemblyManager());
        }

        /// <summary>
        /// Gets all assemblies currently stored in assembly manager
        /// </summary>
        /// <returns>List of all assemblies</returns>
        public List<Assembly> GetAllAssemblies()
        {
            return _assemblies.Values.ToList();
        }

        /// <summary>
        /// Adds assembly to the assembly manager
        /// </summary>
        /// <param name="name">Absolute path to the assembly</param>
        public void AddAssembly(string name)
        {
            if (_resource == null || String.IsNullOrEmpty(name))
                return;

            Assembly assembly = Assembly.LoadFile(name);
            string assemblyName = assembly.GetName().Name;
            if (!_assemblies.ContainsKey(assemblyName))
            {
                _assemblies.Add(assemblyName, assembly);
                AssembliesNames.Add(assemblyName);
                string[] temp = _resource["ComboBoxItems"] as string[];
                if (temp != null)
                {
                    List<string> items = temp.ToList();
                    string typeName = (from tname in assembly.GetTypes()
                                       where tname.GetCustomAttribute<StartClassAttribute>() != null
                                       select tname.ToString()).First();

                    items.Add(typeName + ", " + assemblyName);
                    _resource["ComboBoxItems"] = items.ToArray();
                }
            }
        }

        /// <summary>
        /// Removes assembly from the assembly manager
        /// </summary>
        /// <param name="name">Name of the assembly</param>
        public void RemoveAssembly(string name)
        {
            if (_resource == null || String.IsNullOrEmpty(name))
                return;

            string assemblyName = _assemblies[name].GetName().Name;
            string typeName = (from tname in _assemblies[name].GetTypes()
                               where tname.GetCustomAttribute<StartClassAttribute>() != null
                               select tname.ToString()).First();

            _assemblies.Remove(name);
            AssembliesNames.Remove(name);

            string[] temp = _resource["ComboBoxItems"] as string[];
            if (temp != null)
            {
                string fullName = typeName + ", " + assemblyName;
                List<string> items = temp.ToList();
                items.Remove(fullName);
                _resource["ComboBoxItems"] = items.ToArray();

                AppManager manager = AppManager.GetInstance();
                string nodeType = Properties.Settings.Default["NodeType"] as string;
                string arcType = Properties.Settings.Default["ArcType"] as string;
                if (nodeType == fullName)
                {
                    manager.Change.Option = Utils.ChangeOptions.Node;
                    manager.Change.Execute("int");                   
                }
                if (arcType == fullName)
                {
                    manager.Change.Option = Utils.ChangeOptions.Arc;
                    manager.Change.Execute("int");
                }
            }
        }
    }
}
