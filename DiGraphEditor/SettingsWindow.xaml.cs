﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DiGraphEditor.Properties;
using DiGraphEditor.Utils;
using DiGraphEditor.ViewModel;

namespace DiGraphEditor
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        public Grid GraphGrid { get; private set; }
        public Delegate NodeHandler { get; private set; }
        public Delegate ArcHandler { get; private set; }
        private readonly AppManager _manager;
        private bool _kill;
        private readonly SettingsViewModel _viewModel;

        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsWindow()
        {
            InitializeComponent();
            InitializeComboBoxes();

            _manager = AppManager.GetInstance();
            _viewModel = new SettingsViewModel(Resources);
            DataContext = _viewModel;
            AssembliesList.DataContext = _viewModel.Manager;
        }

        /// <summary>
        /// Initializes combo boxes
        /// </summary>
        private void InitializeComboBoxes()
        {
            string nodeType = (string)Settings.Default["NodeType"] ?? "int";
            string arcType = (string)Settings.Default["ArcType"] ?? "int";
            string[] types = Resources["ComboBoxItems"] as string[];
            if (types != null && !types.Contains(nodeType))
                nodeType = "int";
            if (types != null && !types.Contains(arcType))
                arcType = "int";

            NodeType.SelectedItem = nodeType;
            ArcType.SelectedItem = arcType;
        }

        /// <summary>
        /// Initializes window
        /// </summary>
        /// <param name="aGrid"></param>
        /// <param name="nHandler"></param>
        /// <param name="aHandler"></param>
        public void InitializeWindow(Grid aGrid, Delegate nHandler, Delegate aHandler)
        {
            GraphGrid = aGrid;
            NodeHandler = nHandler;
            ArcHandler = aHandler;
        }

        /// <summary>
        /// Text box validation
        /// </summary>
        /// <param name="sender">A text box to validate</param>
        /// <param name="e">Text composition arguments</param>
        private void TextBoxPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regEx = new Regex("[^0-9.]+");
            e.Handled = regEx.IsMatch(e.Text);
        }

        /// <summary>
        /// Combobox selection changed handler
        /// </summary>
        /// <param name="sender">A combobox</param>
        /// <param name="e">Selection changed arguments</param>
        private void ComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            if (combo != null && _manager != null && combo.SelectedValue != null)
            {
                ChangeOptions option = combo.Name == "NodeType" ? ChangeOptions.Node : ChangeOptions.Arc;
                _manager.Change.Option = option;
                if (_manager.Change.CanExecute(combo.SelectedValue))
                    _manager.Change.Execute(combo.SelectedValue);
            }
        }

        /// <summary>
        /// Closing handler to prevent closing window if the application is still running
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Cancel arguments</param>
        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (!_kill)
            {
                Window win = sender as Window;
                if (win != null) win.Hide();
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Closes the window once and for all
        /// </summary>
        public void Kill()
        {
            _kill = true;
            Close();
        }

        /// <summary>
        /// Ok click handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Routed event arguments</param>
        private void OkClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Add assembly handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Routed event arguments</param>
        private void AddAssemblyClick(object sender, RoutedEventArgs e)
        {
            _viewModel.AddAssemblyClick();
        }

        /// <summary>
        /// Remove assembly handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Routed event arguments</param>
        private void RemoveAssemblyClick(object sender, RoutedEventArgs e)
        {
            string name = (AssembliesList.SelectedItem as string);
            if (name != null)
                _viewModel.RemoveAssemblyClick(name);

            string nodeType = Properties.Settings.Default["NodeType"] as string;
            string arcType = Properties.Settings.Default["ArcType"] as string;
            NodeType.SelectedValue = nodeType;
            ArcType.SelectedValue = arcType;
        }
    }
}
